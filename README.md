### *Chaos* is a linear solver library for the iterative solution of sparse linear systems using Chaotic methods. It is particularly suited for the solution of [Poisson](https://en.wikipedia.org/wiki/Poisson%27s_equation) equations. *Chaos* is designed for massive parallelism and focuses on synchronization-avoiding multigrid solvers. ###

# Download #

* **[Chaos v1.0.0](Chaos-v1.0.0.tar.gz)**
* **[Chaos v1.0.0 (light)](Chaos-v1.0.0-Light.tar.gz)**

The light version includes the full source and examples, but omits some large matrices which are used by some examples. 

# Building #

Prerequisites:

* A C++11 compiler
* OpenMP
* CMake > 2.8

Optional:

* For distributed memory systems, you will need some form of MPI. OpenMPI and Intel MPI have been tested.
* For bindings to other languages you will need their development packages (i.e. Python-Dev), and you will also need 
[Swig](http://www.swig.org/index.php).
* For documentation, you will need Doxygen.

Currently, Swig will generate bindings for Python and Ruby.

To build:

```
#!bash
cd chaos
mkdir build
cd build
cmake ..
make
```
To build additional language bindings:
```
make python
make ruby
```
To build the documentation:
```
make doc
```

# License #

The code is released under the MIT license, see the license file for details.

# Citing #

If you use the *Chaos* library in your work or wish to cite it for other reasons, please cite the following paper:

[Chaotic Multigrid Methods for the Solution of Elliptic Equations](https://www.sciencedirect.com/science/article/pii/S0010465518303916)



```
@article{HAWKES2018,
	title = "Chaotic Multigrid Methods for the Solution of Elliptic Equations",
	journal = "Computer Physics Communications",
	year = "2018",
	issn = "0010-4655",
	doi = "https://doi.org/10.1016/j.cpc.2018.10.031",
	url = "http://www.sciencedirect.com/science/article/pii/S0010465518303916",
	author = "J. Hawkes and G. Vaz and A.B. Phillips and C.M. Klaij and S.J. Cox and S.R. Turnock",
}
```


# Usage #

Some basic examples(in C++, more languages to come) are packaged with the library. These will show you how to set up matrices, vectors and solvers in the different languages. To begin interfacing with your existing software you should check the documentation for the Vector and Matrix classes. Through use of functions like SetRow(...) and ResetRow(...) you can construct your own linear problems to be solved. For a full working example, check example 004, which constructs and solves a Poisson equation using *Chaos*.

# Other Languages #

[Swig](http://www.swig.org/index.php) allows us to wrap many languages, including C#, Java, Perl, Lua, PHP, R, etc. If you wish to add support for these languages you should be able to edit the CMakeLists.txt file and add the necessary targets for these languages.

# Contact #

For help, ideas, suggestions or contributions please contact James Hawkes (jameshawkes@outlook.com).