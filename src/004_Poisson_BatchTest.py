from subprocess import check_output
import os
import re
import matplotlib
import numpy
matplotlib.use('Agg')
import matplotlib.pyplot
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d.axes3d import Axes3D
from matplotlib.ticker import EngFormatter


n = [8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,44,48,54,60,67,72,74,76,78,80,82,84,86,88,90,94,96]
nprocs = [8]
runs = []

for np in nprocs:
	for i in range(8,120):
		#print("Running 004_Poisson with n = " + str(i) + "^3" + " = " + str(i*i*i))
		out = check_output(["mpirun", "-machinefile", "mynodefile3911098.blue101", "-np", str(np), os.getcwd()+"/004_Poisson", str(i), "2>/dev/null"])
		iterations = int(''.join(re.findall ( 'in (.*?) iterations', out)[0] or '0'))
		time = int(''.join(re.findall ( r'\((.*?)ms\)', out)[0] or '0'))
		runs.append({ 'nprocs':np, 'ncells':i*i*i, 'iterations':iterations, 'time':float(time)/1000.0 })

		#print("\t Iterations: " + str(iterations))
		#print("\t Time:       " + str(time) + "ms")
		print (str(i*i*i) + " " + str(iterations) + " " + str(time))

for np in nprocs:
	print(np)
	nCells = []
	iterations = []
	times = []

	for i,r in enumerate(runs):
		if (r['nprocs'] == np):
			nCells.append( float(r['ncells']) )
			iterations.append( float(r['iterations']) )
			times.append( float(r['time']))


	plt.figure(num=1, figsize=(5, 3.75), dpi=80, facecolor='w', edgecolor='k')
	plt.xlabel('N')
	#plt.xscale('log')
	#plt.yscale('log')
	plt.title('')
	plt.ylabel('Iterations')
	plt.plot( nCells, iterations )

	ax = plt.gca()
	formatter = EngFormatter(unit='', places=1)
	ax.xaxis.set_major_formatter(formatter)

	#plt.plot([32,512], [32,512], '--', color='black', label="linear")
	plt.figtext(0.5, 0.8, "np=" +str(np))
	#plt.legend(loc='upper left', prop={'size':8}, frameon=False, ncol=2)
	#savename = 'Poisson_'+str(np)+'_iters.png'
	#plt.savefig(savename, orientation='landscape')
	savename = 'Poisson_'+str(np)+'_iters.pdf'
	plt.savefig(savename, orientation='landscape')
	plt.clf()

	plt.figure(num=1, figsize=(5, 3.75), dpi=80, facecolor='w', edgecolor='k')
	plt.xlabel('N')
	#plt.xscale('log')
	#plt.yscale('log')
	ax = plt.gca()
	formatter = EngFormatter(unit='', places=1)
	ax.xaxis.set_major_formatter(formatter)

	plt.title('')
	plt.ylabel('Time [s]')
	plt.plot( nCells, times )

	#plt.plot([32,512], [32,512], '--', color='black', label="linear")
	plt.figtext(0.5, 0.8, "np=" +str(np))
	#plt.legend(loc='upper left', prop={'size':8}, frameon=False, ncol=2)
	#savename = 'Poisson_'+str(np)+'_time.png'
	#plt.savefig(savename, orientation='landscape')
	savename = 'Poisson_'+str(np)+'_time.pdf'
	plt.savefig(savename, orientation='landscape')
	plt.clf()

