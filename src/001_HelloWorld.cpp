#include <iostream>
#include "chaos.h"

int main () {
	
	int MPIrank = 0;
	int MPIsize = 1;
	Chaos::Init();
	#ifdef CHAOS_MPI_FOUND
		MPI_Comm_rank(MPI_COMM_WORLD, &MPIrank);
		MPI_Comm_size(MPI_COMM_WORLD, &MPIsize);
	#endif

	int offset = MPIrank * 50;
	Matrix<double> mat(50,100, offset);
	int N = mat.Size() * MPIsize;
	
	for ( int i = 0; i < mat.Size(); i++ ) {
		mat.AddRow( 4.0, { (i-1+offset+N)%N, (i+offset+1)%N }, {-1.0, -1.0 } );
	}
	
	mat.Complete();
	
	mat.PrintRow(49);
	
	Vector<double> x ( &mat, 1.0 );
	Vector<double> b ( &mat, 0.1 );
	Vector<double> r ( &mat, 0.0 );
	

	Chaos::Finalize();

}
