#ifndef __CHAOS__
#define __CHAOS__

#include "chaos_private.h"
#include "vector.h"
#include "matrix.h"
#include "solver.h"
#include "parallelcontext.h"

#endif