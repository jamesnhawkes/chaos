#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "chaos_private.h"

#include <string>

#include "vector.h"
#include "parallelcontext.h"

/*template<typename T>
class Vector;
class Vector<int>;
class Vector<double>;*/


/// A class for managing CRS matrices
template<typename T = double>
class Matrix
{
public:
	struct Submatrix {
		int NZ() {return C.Size();}  		///< Number of non-zeroes in the submatrix
		Vector<int> R; 	///< Row pointer, of length (nRows + 1)
		Vector<int> C; 	///< Column index, of length NZ
		Vector<T> V; 	///< Matrix values (excluding diagonal), of length NZ
	};
	/// The local portion of the matrix, excluding diagonal
	Submatrix A;
	/// The off-process portion of the matrix (when using distributed memory)
	Submatrix B;
	/// The diagonal of the matrix
	Vector<T> D;
	
	/// Default constructor for a parallel matrix, global_begin defines the global index at which this processor's block begins
	Matrix( int localRows, int localNZ, int global_begin);

	/// Used when building a matrix and you wish to use automatic, basic partitioning (useful if reading from a file, for example)
	Matrix( int localRows, int localNZ, bool forceSerial = false);
	/// Used when building a matrix from a file, the parallel partitioning is automatic
	Matrix( std::string fileName = "", int NZperRowHint = 0);
	
	/// Adds a row to the local portion of the matrix. Continue adding rows until the matrix is full then call Complete().
	void AddRow( T diag, int nz, int * colIdx, T * values );
	/// Use SetRow to change the values of a matrix after it has been completed. colIdx and nz must be the same as when AddRow was called for this row; the structure of the matrix cannot change.
	void SetRow( int row, T diag, int nz, int * colIdx, T * values );
	void AddLocalRow( T diag, int nz, int * colIdx, T * values );
	void AddRow( T diag, std::vector<int> colIdx, std::vector<T> values );
	void AddLocalRow( T diag, std::vector<int> colIdx, std::vector<T> values );
	void Complete();
	/// Multiplies the matrix by vector mult, placing the answer in the result vector.
	void Multiply(Vector<T> * mult, Vector<T> * result);
	
	~Matrix()
	{
		delete(parallelContext);
	};
	
	
	int Size() { return D.Size(); }
	void PrintRow(int i);
	void Print();
	T at(int row, int col) ;
	ParallelContext * GetParallelContext() { return parallelContext; }
	
protected:
	void Allocate(int localRows, int localNZ);
	ParallelContext * CreateParallelContext();
	ParallelContext * parallelContext;
	
	
	//bool isLocked = false;
	
	bool isParallel;
	
	//int GetProc(int colIdx, int * start_end_array);

private:
	int rowCounter;
};

#endif