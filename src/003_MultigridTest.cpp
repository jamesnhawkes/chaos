#include <iostream>
#include "chaos.h"

int main () {
	

	Chaos::Init();
	Chaos::SetLogLevel(4);
	
	Matrix<> mat("../matrices/massTransport00005.mat");
	Vector<> x("../matrices/massTransport00005.x", &mat);
	Vector<> b("../matrices/massTransport00005.rhs", &mat);
	Vector<> r(&mat, 0.0);
	
	Vector<> original = x;
	// double meanb = b.Sum()/b.Size();
	// for (int i = 0; i < b.Size(); i++)
	// 	b[i] -= meanb;
		
	// Diagonal perturbation
	// Patankar relaxation or E-factor  -- page 540
	//https://books.google.co.uk/books?id=GYRgCgAAQBAJ&pg=PA541&lpg=PA541&dq=implicit+relaxation+diagonal+cfd&source=bl&ots=z1Pq7FLbKC&sig=dr_F-KLt1IKK2xbm8CopIMU4iHU&hl=en&sa=X&ved=0ahUKEwiz6tqhuo7LAhWGPj4KHatBA-sQ6AEIHDAA#v=onepage&q=implicit%20relaxation%20diagonal%20cfd&f=false
	// double implicit_relaxation = 0.9999;
	// for (int i = 0; i < b.Size(); i++)
	// {
	// 	mat.D[i] *= 1/implicit_relaxation;
	// 	b[i] += mat.D[i]*(1-implicit_relaxation)/implicit_relaxation;
	// }
		
	
	// Matrix scaling
	/*for (int i = 0; i < mat.Size(); i++) {
		for (int j = mat.A.R[i]; j < mat.A.R[i+1]; j++)
			mat.A.V[j] /= mat.D[i];
		b[i] /= mat.D[i];
		mat.D[i] = 1.0;
	}*/
	
	Solver s;
	s.SetResidualVector ( &r );
	s.SetRelativeConvergenceTolerance( 5e-1 );
	s.SetMaxIterations( 100000 );

	s.SetPostSmoothIterations(6);
	s.SetPreSmoothIterations(6);
    s.SetSolverType ( Solver::SolverType::MG_V );

	s.SetSolution( &x );
	s.SetRightHandSide( &b );
	
	s.SetMatrix( &mat, false );
	s.Solve();

	x = original;
	s.SetMatrix( &mat, false );
	s.Solve();

	x = original;
	s.SetMatrix( &mat, false );
	s.Solve();

	x = original;
	s.SetMatrix( &mat, false );
	s.Solve();

	x = original;
	s.SetMatrix( &mat, true );
	s.Solve();

	x = original;
	s.SetMatrix( &mat, false );
	s.Solve();

	x = original;
	s.SetMatrix( &mat, false );
	s.Solve();


	//s.SetMatrix( &mat, true );

		
	//for (int i = 0; i < r.Size(); i++)
	//	std::cout << r[i] << std::endl;
	
	//std::cout << s.norm/s.norm0 << " in " << s.iterations << " iterations (" << toc - tic << "secs)" << std::endl;
	
	Chaos::Finalize();

}
