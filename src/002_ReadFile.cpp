#include <iostream>
#include "chaos.h"

int main () {
	
	Chaos::Init();
	
	Matrix<> mat("../matrices/massTransport00005.mat");
	Vector<> x("../matrices/massTransport00005.x", &mat);
	Vector<> b("../matrices/massTransport00005.rhs", &mat);
	
	Vector<> r(&mat, 0.0);
	
	// TODO: Add smoother
	//Solver_GaussSeidel s ( &mat, &x, &b, &r, opts );
	//s.Solve();
	
	Chaos::Finalize();

}
