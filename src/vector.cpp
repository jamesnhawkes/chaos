#include "vector.h"

#include "chaos.h"

#include "matrix.h"

#include <fstream>
#include <math.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <sstream>

template<typename T>
Vector<T>::Vector(int size)
{
	isParallel = false;
	Resize(size);
}

template<typename T>
Vector<T>::Vector ( int size, T * values )
{
	isParallel = false;
	Resize(size);
	SetFromArray(values);
}

template<typename T>
Vector<T>::Vector ( int size, T value )
{
	isParallel = false;
	Resize(size);
	SetAll(value);
}

template<typename T>
Vector<T>::Vector ( ParallelContext * parallelContext )
{
	SetParallelContext(parallelContext);
}

template<typename T>
Vector<T>::Vector ( Matrix<T> * associated_mat )
	: Vector<T>( associated_mat->GetParallelContext() )
{}

template<typename T>
Vector<T>::Vector ( ParallelContext * parallelContext, T * values )
{
	isParallel = false;
	SetParallelContext(parallelContext);
	SetFromArray(values);
}

template<typename T>
Vector<T>::Vector ( Matrix<T> * associated_mat, T * values )
	: Vector<T>( associated_mat->GetParallelContext(), values )
{}

template<typename T>
Vector<T>::Vector ( ParallelContext * parallelContext,  T value )
{
	SetParallelContext(parallelContext);
	SetAll(value);
}

template<typename T>
Vector<T>::Vector ( Matrix<T> * associated_mat,  T value )
	: Vector<T>( associated_mat->GetParallelContext(), value )
{}

template<typename T>
Vector<T>::Vector ( std::string fileName )
{
	// Read the size of the Vector
	int size;
	std::ifstream infile(fileName);
	infile >> size;
	Resize(size);
	
	// Read the values
	std::string junk;
	for (int i = 0; i < Size(); ++i) {
		infile >> junk >> A[i];
	}
	infile.close();
}

template<typename T>
Vector<T>::Vector ( std::string fileName, 	ParallelContext * parallelContext )
{
	SetParallelContext(parallelContext);
	
	std::ifstream infile(fileName);
	T junkval;
	std::string junkstr;
	infile >> junkval;
	for (int i = 0; i < parallelContext->rangeBottom; i++ ) { infile >> junkstr >> junkval; }
	for (int i = 0; i < parallelContext->size; i++) {
		infile >> junkstr >> A[i];
	}
	infile.close();
	Scatter();
}

template<typename T>
Vector<T>::Vector ( std::string fileName, Matrix<T> * associated_mat )
	: Vector<T>( fileName, associated_mat->GetParallelContext() )
{}

template<typename T>
void Vector<T>::SetFromArray(T * values)
{
	for(int i = 0; i < A.size(); i++)
		A[i] = values[i];
	Scatter();
}

template<typename T>
void Vector<T>::Scatter()
{
	ScatterBegin();
	ScatterEnd();
}

template<typename T>
void Vector<T>::ScatterBegin()
{
	#ifdef CHAOS_MPI_FOUND
		// Pack the required rows into the send_buf
		for (int i = 0; i < sendbuf.size() ; i++){
			sendbuf[i] = A[ parallelContext->send_buf_cols[i] ];
		}


		// For each proc who's message > 0, send a message
		for (int proc = 0; proc < parallelContext->MPIsize; proc++) {
			int messageSize = parallelContext->send_buf_proc_offset[proc+1] - parallelContext->send_buf_proc_offset[proc];
			if (messageSize > 0) {
				/*std::cout << "Rank " << parallelContext->MPIrank << " scattering " << messageSize << " elements to proc " << proc
					<< " ( at " << parallelContext->send_buf_proc_offset[proc] << ")" << std::endl;*/
				MPI_Request r;
				MPI_Isend( &sendbuf[parallelContext->send_buf_proc_offset[proc]], messageSize*sizeof(T), MPI_BYTE, proc, 0, MPI_COMM_WORLD, &r);
				MPI_Request_free(&r);
				//MPI_Wait(&r, MPI_STATUS_IGNORE);
			}
		}
	#endif
}

template<typename T>
void Vector<T>::ScatterEnd()
{
	#ifdef CHAOS_MPI_FOUND
		irequests.clear();
		// For each proc who's message > 0, receive a message
		// Place it in the recv_buf
		for (int proc = 0; proc < parallelContext->MPIsize; proc++) {
			int messageSize = parallelContext->rec_buf_proc_offset[proc+1] - parallelContext->rec_buf_proc_offset[proc];
			if (messageSize > 0) {
				MPI_Request r;
				MPI_Irecv( &B[parallelContext->rec_buf_proc_offset[proc]], messageSize*sizeof(T), MPI_BYTE, proc, 0, MPI_COMM_WORLD, &r);
				irequests.push_back(r);
				
				//if (scatter->mpiRank == 1) {
					// std::cout << "Rank " << scatter->mpiRank << " receiving " << messageSize << " elements from proc " << proc << " ( at " << scatter->rec_buf_proc_offset[proc] << ")";
					// std::cout << std::endl << "{ ";
					// for (int i = 0; i < messageSize; ++i)
					// {
					// 	std::cout << remote.at(scatter->rec_buf_proc_offset[proc] + i) << " ";
					// }
					// std::cout << " }" << std::endl;
				//}
			}
		}

		MPI_Waitall( irequests.size(), &irequests[0], MPI_STATUSES_IGNORE );
		irequests.clear();
	#endif
}

template<typename T>
void Vector<T>::ScatterBeginProxy(int coarse, std::vector<T> & source, std::vector<std::vector<std::vector<int>>> & fcMaps)
{
	#ifdef CHAOS_MPI_FOUND

		// Pack the required rows into the send_buf
		for (int i = 0; i < sendbuf.size() ; i++){
			int idx = fcMaps[coarse][0][ parallelContext->send_buf_cols[i] ];
			sendbuf[i] =  source[idx];
			//if (idx >= source.size()) std::cout << coarse  << ": IDX: " << idx << " vs size " << source.size() << std::endl;
		}
		// For each proc who's message > 0, send a message with tag coarse
		for (int proc = 0; proc < parallelContext->MPIsize; proc++) {
			int messageSize = parallelContext->send_buf_proc_offset[proc+1] - parallelContext->send_buf_proc_offset[proc];
			if (messageSize > 0) {
				/*std::cout << "Rank " << parallelContext->MPIrank << " scattering " << messageSize << " elements to proc " << proc
					<< " ( at " << parallelContext->send_buf_proc_offset[proc] << ")" << std::endl;*/
				MPI_Request r;
				MPI_Isend( &sendbuf[parallelContext->send_buf_proc_offset[proc]], messageSize*sizeof(T), MPI_BYTE, proc, coarse, MPI_COMM_WORLD, &r);
				MPI_Request_free(&r);
				//MPI_Wait(&r, MPI_STATUS_IGNORE);
			}
		}
	#endif
}

template<typename T>
void Vector<T>::ScatterEndProxy(int coarse, std::vector<T> & destination)
{
	#ifdef CHAOS_MPI_FOUND
		irequests.clear();
		// For each proc who's message > 0, receive a message
		// Place it in the recv_buf
		for (int proc = 0; proc < parallelContext->MPIsize; proc++) {
			int messageSize = parallelContext->rec_buf_proc_offset[proc+1] - parallelContext->rec_buf_proc_offset[proc];
			if (messageSize > 0) {
				//MPI_Status s;
				MPI_Request r;
				MPI_Irecv( &destination[parallelContext->rec_buf_proc_offset[proc]], messageSize*sizeof(T), MPI_BYTE, proc, coarse, MPI_COMM_WORLD, &r);
				irequests.push_back(r);
				
				//if (scatter->mpiRank == 1) {
					// std::cout << "Rank " << scatter->mpiRank << " receiving " << messageSize << " elements from proc " << proc << " ( at " << scatter->rec_buf_proc_offset[proc] << ")";
					// std::cout << std::endl << "{ ";
					// for (int i = 0; i < messageSize; ++i)
					// {
					// 	std::cout << remote.at(scatter->rec_buf_proc_offset[proc] + i) << " ";
					// }
					// std::cout << " }" << std::endl;
				//}
			}
		}
		MPI_Waitall( irequests.size(), &irequests[0], MPI_STATUSES_IGNORE );
		irequests.clear();

	#endif
}

template <typename T>
void Vector<T>::SetParallelContext( ParallelContext * parallelContext )
{
	this->parallelContext = parallelContext;
	isParallel = true;
	A.resize(parallelContext->size);
	B.resize(parallelContext->rec_buf_size);
	sendbuf.resize(parallelContext->send_buf_size);
}

template <typename T>
void Vector<T>::Resize(int size)
{
	if (!isParallel) {
		A.reserve(size);
		A.resize(size);
		return;
	}
	#ifdef CHAOS_DEBUG
	std::cout << "Error: Cannot resize a parallel Vector, size is fixed by the ParallelContext." << std::endl;
	exit(EXIT_FAILURE);
	#endif
}

template <typename T>
void Vector<T>::Reserve(int size)
{
	if (!isParallel) {
		A.reserve(size);
		return;
	}
	#ifdef CHAOS_DEBUG
	std::cout << "Error: Cannot reserve size for a parallel Vector, size is fixed by the ParallelContext." << std::endl;
	exit(EXIT_FAILURE);
	#endif
}

template <typename T>
void Vector<T>::Add(T value)
{
	if (!isParallel) {
		A.push_back(value);
		return;
	}
	#ifdef CHAOS_DEBUG
	std::cout << "Error: Cannot add to a parallel Vector, size is fixed by the ParallelContext." << std::endl;
	exit(EXIT_FAILURE);
	#endif
}

template <typename T>
void Vector<T>::Optimize()
{
	// TODO: Not compatible with Intel 16 which does not have a full c++11 implementation
	//A.shrink_to_fit();
}

template <typename T>
T Vector<T>::NormLocal()
{
	T val = 0.0;
	for (int i = 0; i < Size(); i++) {
		val += A[i] * A[i];
	}
	return sqrt(val);
}

template <typename T>
T Vector<T>::SumLocal()
{
	T val = 0.0;
	for (int i = 0; i < Size(); i++) {
		val += A[i];
	}
	return val;
}

template<typename T>
T Vector<T>::Norm() {
	/// TODO: This function only works for T = double due to the MPI type "MPI_DOUBLE"
	#ifdef CHAOS_MPI_FOUND
		T myVal = 0.0;
		for (int i = 0; i < Size(); i++) {
			myVal += A[i] * A[i];
		}
		T allVal = 0.0;
		MPI_Allreduce( &myVal, &allVal, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		return sqrt(allVal);
	#else
		return NormLocal();
	#endif
}

template<typename T>
void Vector<T>::NormBegin() {
	/// TODO: This function only works for T = double due to the MPI type "MPI_DOUBLE"
	#ifdef CHAOS_MPI_FOUND
		myVal = 0.0;
		for (int i = 0; i < Size(); i++) {
			myVal += A[i] * A[i];
		}
		allVal = 0.0;
		MPI_Iallreduce( &myVal, &allVal, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, &request);
	#endif
}

template<typename T>
T Vector<T>::NormEnd() {
	/// TODO: This function only works for T = double due to the MPI type "MPI_DOUBLE"
	#ifdef CHAOS_MPI_FOUND
		MPI_Wait(&request, MPI_STATUS_IGNORE);		
		return sqrt(allVal);
	#else
		return NormLocal();
	#endif
}


template<typename T>
T Vector<T>::Sum() {
	/// TODO: This function only works for T = double due to the MPI type "MPI_DOUBLE"
	#ifdef CHAOS_MPI_FOUND
		T allSum = 0.0;
		T mySum = SumLocal();
		MPI_Allreduce( &mySum, &allSum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		return allSum;
	#else
		return SumLocal();
	#endif
}

template <typename T>
void Vector<T>::SetAll(T value)
{
	std::fill(A.begin(), A.end(), value);
	std::fill(B.begin(), B.end(), value);
}


template class Vector<double>;
template class Vector<int>;
