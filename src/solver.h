#ifndef SOLVER_H
#define SOLVER_H

#include "chaos_private.h"

#include <string>
#include <vector>
//#include <atomic>
#include <iostream>
#include <chrono>
#include "omp.h"

#include "matrix.h"
#include "vector.h"
#include "chaos.h"

using std::string;


class Solver
{
public:

	// Ctor and Dtor
	Solver() {
		name = "NoName";
		Reset();
	}
	Solver (std::string name) : name(name) {
		Reset();
	}
	Solver(Matrix<double> * A,  Vector<double> * x, Vector<double> * b, Vector<double> * r, std::string name = ""): A(A), x(x), b(b), r(r), name(name) {
		Reset();
	}
	~Solver(){
		for (int i = 1; i < mats.size(); i++) {
			delete( mats[i] );
			delete( xs[i] );
			delete( rs[i] );
			delete( bs[i] );
			delete( work1[i] );
		}
		if (work1.size() > 0 )
			delete work1[0];
		mats.clear();
		xs.clear();
		rs.clear();
		bs.clear();
		work1.clear();
		cfMaps.clear();
	};

	// Enums
	enum class SolverType {
		MG_W, 					/*!< Classical Multigrid W-Cycle */
		MG_V,					/*!< Classical Multigrid V-Cycle */
		MG_F,					/*!< Classical Multigrid F-Cycle */
		MG_Sawtooth,			/*!< Classical Multigrid Sawtooth-Cycle */
		MG_Chaotic,				/*!< Chaotic Multigrid Cycle */
		Chaotic 				/*!< Non-Multigrid Chaotic */
	};

	/// Sets the type of solver and cycling to use (i.e. V-cycle, W-cycle, Chaotic-cycle, GaussSeidel, Chaotic)
	void SetSolverType (SolverType solverType) { this->solverType = solverType; }


	/**
	 * @brief      Sets the relative convergence tolerance based on the L2-norm of the residual vector.
	 *
	 * @param[in]  tol        The tolerance, relative to the initial residual.
	 */
	void SetRelativeConvergenceTolerance(double tol) { rtol = tol; }

	/**
	 * @brief      Sets the absolute convergence tolerance based on the L2-norm of the residual vector.
	 *
	 * @param[in]  tol        The absolute tolerance.
	 */
	void SetAbsoluteConvergenceTolerance(double tol) { atol = tol; }


	/**
	 * @brief      Sets the maximum iterations.
	 *
	 * @param[in]  iterations  The iterations
	 */
	void SetMaxIterations(int iterations) { maxIterations = iterations; }

	/**
	 * @brief      Sets the relative divergence tolerance based on the L2-norm of the residual vector. If this value is exceeded, the solver terminates without finding a solution.
	 *
	 * @param[in]  tol        The tolerance, relative to the initial residual.
	 */
	void SetRelativeDivergenceTolerance(double tol) { rdtol = tol; }

	/**
	 * @brief      Sets the absolute divergence tolerance based on the L2-norm of the residual vector. If this value is exceeded, the solver terminates without finding a solution.
	 *
	 * @param[in]  tol        The absolute tolerance.
	 */
	void SetAbsoluteDivergenceTolerance(double tol) { adtol = tol; }
	

	/**
	 * @brief      Sets the number of pre-smoothing iterations to be performed before transferring to a coarser grid.
	 *
	 * @param[in]  iterations  Number of iterations
	 */
	void SetPreSmoothIterations (int iterations) { preSmoothIterations = iterations; }


	/**
	 * @brief      Sets the number of post-smoothing iterations to be performed before transferring to a finer grid.
	 *
	 * @param[in]  iterations  Number of iterations
	 */
	void SetPostSmoothIterations (int iterations) { postSmoothIterations = iterations; }


	/**
	 * @brief      Sets the number of smoother iterations to be performed on the coarsest level
	 *
	 * @param[in]  iterations  The iterations
	 */
	void SetCoarsestSmoothIterations (int iterations) { coarsestSmoothIterations = iterations; }


	/**
	 * @brief      Sets the maximum number of levels to be created. Default is 10.
	 *
	 * @param[in]  levels     Number of levels
	 */
	void SetMaxLevels(int levels) { maxLevels = levels; }


	/**
	 * @brief      Sets the minimum size of the coarsest grid, limiting the number of grids that are created.
	 *
	 * @param[in]  size       The size
	 */
	void SetMinimumGridSize(int size) { minimumGridSize = size; if (minimumGridSize < 2) minimumGridSize = 2; }

	/**
	 * @brief      Sets the minimum size of the coarsest grid as a ratio of the finest grid (i.e. 0.1), limiting the number of grids that are created.
	 * Setting this value to 0.0 effectively removes this check.
	 *
	 * @param[in]  ratio      The ratio
	 */
	void SetMinimumGridSizeRelative(double ratio) { minimumGridSizeRelative = ratio; }


	/**
	 * @brief      Sets the minimum coupling coefficient for the pairwise aggregation process. Usually 0.25.
	 * Any coupling less than this value is disregarded as it is considered not strong enough to justify aggregation.
	 *
	 * @param[in]  value  The value
	 */
	void SetStrongCouplingCoefficient(double value = 0.25) { strongCouplingCoefficient = value; }

	
	/**
	 * @brief      Gets the number of iterations performed by the solver during Solve().
	 */
	int GetIterations() { return iterations; }
	
	/**
	 * @brief      Gets the L2 norm of the starting residual from the last Solve().
	 *
	 * @return
	 */
	double GetNorm0() { return norm0; }

	/**
	 * @brief      Gets the L2 norm of the residual from the last Solve().
	 *
	 * @return
	 */
	double GetNorm() { return norm; }

	/**
	 * @brief      Calculates the residual vector and returns the L2-norm of the residual.
	 *
	 * @param[in]  doScatter  If true, the solver will update halo data in the solution vector prior to computing the residual. Usually not required.
	 *
	 * @return     The L2-norm of the residual vector.
	 */
	double ComputeResidual(bool doScatter = false);
	void BeginComputeResidual(bool doScatter = false);
	double EndComputeResidual();

	/**
	 * @brief      Computes the residual of a single row in the system.
	 *
	 * @param[in]  rowIdx  The row index in local coordinates
	 *
	 * @return     The residual.
	 */
	inline double ComputeRowResidual(int rowIdx, int level = 0)
	{
		if (level == 0) {
			double rowSum = 0;
			for (int j = mats[level]->A.R[rowIdx]; j < mats[level]->A.R[rowIdx+1]; j++) {
				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
			}
			for (int j = mats[level]->B.R[rowIdx]; j < mats[level]->B.R[rowIdx+1]; j++ ) {
				rowSum += mats[level]->B.V[j] * xs[level]->B[ mats[level]->B.C[j] ];
			}
			rowSum += mats[level]->D[rowIdx] * xs[level]->A[rowIdx];
			return (bs[level]->A[rowIdx] - rowSum);
		} else {
			double rowSum = 0;
			for (int j = mats[level]->A.R[rowIdx]; j < mats[level]->A.R[rowIdx+1]; j++) {
				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
			}
			for (int fi = 0; fi < cfMaps[level][0][rowIdx].size(); fi++) {
				int f_idx = cfMaps[level][0][rowIdx][fi];
				for (int fj = mats[0]->B.R[  f_idx  ]; fj < mats[0]->B.R[f_idx+1]; fj++ ) {
					rowSum += mats[0]->B.V[fj] * xs[level]->B[ mats[0]->B.C[fj] ];
				}
			}
			rowSum += mats[level]->D[rowIdx] * xs[level]->A[rowIdx];
			return (bs[level]->A[rowIdx] - rowSum);

		}
	}

	
	/**
	 * @brief      Sets the matrix (A) in the Ax=b system.
	 *
	 * @param      A
	 * @param      reuseAggregation If true, a previous aggregation will be used to fill coarse matrices (useful if the matrix has not changed significantly). If false, a new 
	 * aggregation will be performed, creating coarse grids as necessary (relatively expensive).
	 */
	void SetMatrix		  	( Matrix<double> * A, bool reuseAggregation = false );

	/**
	 * @brief      Sets the right hand side vector (b) in the Ax=b system.
	 *
	 * @param      b
	 */
	void SetRightHandSide 	( Vector<double> * b ) { this->b = b; }

	/**
	 * @brief      Sets the solution vector (x) in the Ax=b system..
	 *
	 * @param      x
	 */
	void SetSolution 	 	( Vector<double> * x ) { this->x = x; }
	
	/**
	 * @brief      Sets the residual vector, (r = b - Ax).
	 *
	 * @param      r
	 */
	void SetResidualVector	( Vector<double> * r ) { this->r = r; }

	/**
	 * @brief      The maximum coarsening between levels of the aggregation. Default 4 will aim to reduce the grid size by a factor of 4 per step.
	 *
	 * @param[in]  factor
	 */
	void SetAggregationFactor(int factor = 4) { aggregationFactor = factor; }

	
	/**
	 * @brief      Runs the solver.
	 *
	 * @return     Returns true if the solver converged, false otherwise.
	 */
	bool Solve();

protected:
	/// Reference to the original matrix
	Matrix<double> * A;
	/// Reference to the original solution, rhs and residual vectors
	Vector<double> * x, * b, * r;

	/// Identifier string
	std::string name;

	/// Relative convergence tolerance
	double rtol;
	/// Relative divergence tolerance
	double rdtol;
	/// Absolute convergence tolerance
	double atol;
	/// Absolute divergence tolerance
	double adtol;
	/// Maximum number of iterations
	int maxIterations;

	/// Number of iterations performed by the solver
	int iterations;
	/// The initial residual of the Ax=b system
	double norm0;
	/// The current residual of the Ax=b system
	double norm;

	/// The type of solver to use
	SolverType solverType;

	/// Check whether the iterative process has converged
	bool IsConverged();
	/// Check whether the iterative process has diverged
	bool IsDiverged();

	/// Strong coupling coefficient used in the pairwise aggregation process
	double strongCouplingCoefficient;
	
	/// Array of matrices (0 = original, finest grid)
	std::vector< Matrix<double> * > mats;
	/// Array of solution vectors (x) vectors (0 = original, finest grid)
	std::vector< Vector<double> * > xs;
	/// Array of right-hand side (b) vectors (0 = original, finest grid)
	std::vector< Vector<double> * > bs;
	/// Array of residual (r) vectors (0 = original, finest grid)
	std::vector< Vector<double> * > rs;
	/// Array of work vectors for Jacobi smoothers
	std::vector< Vector<double> * > work1;

	/*! Stores the coarse-to-fine mappings between matrices.
	 *  
	 *  This is a 4-dimensional matrix, the first index is
	 *  the coarse mesh you are ON, the 2nd index is the fine
	 *  mesh to map TO. The 3rd index is the row of the coarse
	 *  mesh, and the final dimension can be iterated on to get
	 *  each index.
	 * 
	 *  i.e. The following example gets the indices in matrix[0]
     *  of the 100th row in matrix[1].
	 * 
	 *  for (int i = 0; i < cfMaps[1][0][100].size(); i++)
	 *     index = cfMaps[1][0][100][i];
	 */
	std::vector<std::vector<std::vector<std::vector<int>>>> cfMaps;
	std::vector<std::vector<std::vector<int>>> fcMaps;
	
	/// Performs the full aggregration process, creating all necessary levels
	void PerformAggregation();

	/// Using a previous aggregation, updates the coarse matrix values.
	void FillCoarseGridsUsingPreviousAggregation();



	/// Helper class for aggregation, provides functions to determine coupling of a single element (node) in the fine matrix
	class Node;
	/// Helper class for aggregation, data structure which represents an aggregate of multiple fine-matrix elements, which becomes a coarse matrix element.
	class Aggregate;
	class Aggregate2;

	/// Creates a coarse matrix using pairwise aggregation
	Matrix<double> * DoAggregation( Matrix<double> * fine );
	Matrix<double> * DoAggregation2( Matrix<double> * fine );
	Matrix<double> * MakeMatrixFromAggregation(Matrix<double> * fine, std::vector<Aggregate2*> & aggregates, std::vector<Aggregate2*> & unique_aggregates);

	void ComputeSolverLevels();

	inline void Restrict(int level) {}
	inline void Prolong(int level) {}
	inline void Smooth(int level, int numIterations) {}

	
    bool ChaoticSolve();

    bool ChaoticMGSolve();
    bool ChaoticMGSolve_OLD();

    bool ClassicalMGSolve();
    void ClassicalMGSmooth( int level, int iterations, bool isParallel = true );
    void ClassicalMGSmooth_Jacobi( int level, int iterations, bool isParallel = true );
    void ClassicalMGRestrict(int f, int c);
    void ClassicalMGSawtoothRestrict(int f, int c);
    void ClassicalMGProlong(int c, int f);
    void ClassicalMGRecursiveSolve(int level, std::vector<bool> * HasVisited = NULL);

    int IncrementCounterIfCounts(std::vector<int> & thread_its, int & thread, int & barrier_flip);
    bool DoesCount(std::vector<int> & thread_its, int & thread, int & barrier_flip);
	int IncrementCounter(std::vector<int> & thread_its, int & thread, int & barrier_flip);

	/// The current level the solver is on
	int currentLevel;
	/// The maximum number of levels to be produced
	int maxLevels;
	/// The minimum grid size to be created
	int minimumGridSize;
	/// The minimum grid size ratio
	double minimumGridSizeRelative;
	/// The number of levels that have been created
	int numLevels;
	/// The number of pre-smoothing iterations to perform
	int preSmoothIterations;
	/// The number of post-smoothing iterations to perform
	int postSmoothIterations;
	/// The number of smoothing steps to perform on the coarsest grid
	int coarsestSmoothIterations;

	/// The coarsest level visited by the solver
	int coarsestSolverLevel;

	/// The number of levels visited by the solver
	int numSolverLevels;

	/// The number of levels to skip during the multigrid cycle (default = 1, no skipping). s=2 will use levels 0,2,4,6, etc.
	int s;

	/// The aggregation factor is the ideal coarsening between levels of the multigrid aggregation process. 
	int aggregationFactor;

	/**
	 * @brief      Resets all options and solver parameters. References to matrix/vectors are kept.
	 */
	void Reset();


	// A C++11 barrier of variable size, allows barriering with just some of the available threads
	struct bar_t {
	    unsigned const count;
	    unsigned spaces;
	    unsigned generation;
	    bar_t(unsigned count_) :
	        count(count_), spaces(count_), generation(0)
	    {}
	    void Wait() {
	        unsigned const my_generation = generation;

	        #pragma omp critical (_barrier)
	        if (--spaces == 0)
	        {
	        	// Executed by the last (slowest) thread
	            spaces = count;
	            #pragma omp flush
	            ++generation;
	            #pragma omp flush
	        }

            while(generation == my_generation)
            {
            	#pragma omp flush
            	;
            }

            //std::cout << "Thread stop at gen " << my_generation << std::endl;

	    }
	    inline unsigned BeginWait() {
	    	unsigned const my_generation = generation;
	    	#pragma omp critical (_barrier)
	    	{
	    		--spaces;
	    	}
	        return my_generation;
	    }
	    inline bool EndWait(unsigned id) {
	    	unsigned const my_generation = id;
	    	bool done = false;
	    	#pragma omp flush
	    	#pragma omp critical (_barrier)
	        if (spaces == 0)
	        {
	        	// Executed by the last (slowest) thread
	            spaces = count;
	            #pragma omp flush
	            ++generation;
	            #pragma omp flush
	            done = true;
	        } else if (generation > my_generation) {
	        	done = true;
	        } else {
	        	done = false;
	        }
	        return done;
	    }
	};
    void ChaoticSawtoothRestrict(int bottom_coarse, int top_coarse, bar_t & comp_barrier);

	inline void RelaxRow(Matrix<double> * mat, Vector<double> * x, Vector<double> * rhs, int row);
	inline void RelaxRowNoComm(Matrix<double> * mat, Vector<double> * x, Vector<double> * rhs, int row);
	inline void Smooth(int level, int start, int end);
	inline void SmoothNoComm(Matrix<double> * mat, Vector<double> * x, Vector<double> * rhs, int start, int end);

	
private:
};


#endif // SOLVER_H
