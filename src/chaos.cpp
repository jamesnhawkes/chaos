#include "chaos_private.h"

bool Chaos::didInitializeMPI = false;
std::chrono::time_point<std::chrono::system_clock> Chaos::tic, Chaos::toc;
int Chaos::logLevel = 0;