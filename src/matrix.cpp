#include "matrix.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include <iterator>

#include "vector.h"


/*template<typename T>
Matrix<T>::Submatrix
{ int NZ; Vector<int> R; Vector<int> C; Vector<T> V; }*/

template <typename T>
void Matrix<T>::PrintRow(int i)
{

    //std::cout << B.R[i] << " -> " << B.R[i+1] << std::endl;

    std::cout << parallelContext->MPIrank << ": " << i << ": (Diag, " << D[i] << ")";
    for (int j = A.R[i]; j < A.R[i+1]; j++)
	std::cout << "(" << A.C[j] << ", " << A.V[j] << ")";
    for (int j = B.R[i]; j < B.R[i+1]; j++)
	std::cout << "(*" << B.C[j] << "*, " << B.V[j] << ")";
    std::cout << std::endl;
}

template <typename T>
void Matrix<T>::Print()
{
	for (int i = 0; i < Size(); i++)
		PrintRow(i);
}

template <typename T>
Matrix<T>::Matrix(int localRows, int localNZ, int global_begin)
{
	parallelContext = new ParallelContext(localRows, global_begin);
	if (parallelContext->MPIsize > 0)
		isParallel = true;
	else
		isParallel = false;
	Allocate(localRows, localNZ);
	rowCounter = 0;
}

template <typename T>
Matrix<T>::Matrix(int localRows, int localNZ, bool forceSerial)
{
	parallelContext = new ParallelContext(localRows, forceSerial);
	if (parallelContext->MPIsize > 0)
		isParallel = true;
	else
		isParallel = false;
	Allocate(localRows, localNZ);
	rowCounter = 0;
}

template <typename T>
void Matrix<T>::Allocate(int localRows, int localNZ) {

	// Resize the vector which holds the diagonal
	D.Resize(localRows);

	
	// Resize the local matrix (A) and reserve memory for colIdx and values
	A.R.Resize( Size() + 1 );
	A.R.SetAll(-1);
	A.R[0] = 0;
	A.C.Reserve(localNZ);
	A.V.Reserve(localNZ);
	
	if (isParallel) {
		// ... and then the off-process matrix (B)
		B.R.Resize( Size() + 1 );
		B.R.SetAll(-1);
		B.R[0] = 0;
		B.C.Reserve(localNZ/10); // Assuming approx 10% off-process
		B.V.Reserve(localNZ/10);
	}
}


template<typename T>
void Matrix<T>::AddRow( T diag, int nz, int * colIdx, T * values )
{
	// Set diagonal
	//std::cout << parallelContext->rangeTop << std::endl;
	D[rowCounter] = diag;
	
	for(int i = 0; i < nz; i++) {
		
		int C_global = colIdx[i];
		int C_local = colIdx[i] - parallelContext->rangeBottom;
			
		// If in the local matrix (A)
		if ( C_global >= parallelContext->rangeBottom && C_global <= parallelContext->rangeTop ) {
			// If not the diagonal (this is set separately, ignored if found in the arrays
			if ( C_local != rowCounter ) {
				// Add the column index
				A.C.Add( C_local );
				// Add the value
				//std::cout << "Adding V" << std::endl;
				A.V.Add( values[i] );
				// Set the rowPtr for the next row to begin at the end of the A.C/A.V vector 
				//std::cout << "Setting Row" << std::endl;
				A.R[ rowCounter + 1 ] = A.C.Size();
			}
		} else // In the offproc matrix (B)
		{
			//std::cout << C_global << " not on " << parallelContext->MPIrank << std::endl;
			// Add the column index (global, we're going to update this to point to a buffer later
			B.C.Add( C_global );
			// Add the value
			B.V.Add( values[i] );
			// Set the rowPtr for the next row to begin at the end of the B.C/B.V vector 
			B.R[ rowCounter + 1 ] = B.C.Size();
		}
	}
	rowCounter++;
}

template<typename T>
void Matrix<T>::SetRow(int row, T diag, int nz, int * colIdx, T * values ) {

	// colIdx and nz should be identical to when AddRow was called
	int A_offset = 0;
	int B_offset = 0;

	D[row] = diag;
	for(int i = 0; i < nz; i++) {
		
		int C_global = colIdx[i];
		int C_local = colIdx[i] - parallelContext->rangeBottom;
			
		// If in the local matrix (A)
		if ( C_global >= parallelContext->rangeBottom && C_global <= parallelContext->rangeTop ) {
			// If not the diagonal (this is set separately, ignored if found in the arrays
			if ( C_local != rowCounter ) {
				A.V [A.R[row]+A_offset] = values[i];
				A_offset++;
			}
		} else // In the offproc matrix (B)
		{

			B.V [B.R[row]+B_offset] = values[i];
			B_offset++;
		}
	}
}

template<typename T>
void Matrix<T>::AddLocalRow( T diag, int nz, int * colIdx, T * values )
{
	#pragma omp critical
	{
		// Set diagonal
		//std::cout << parallelContext->rangeTop << std::endl;
		D[rowCounter] = diag;
		
		for(int i = 0; i < nz; i++) {
			
			int C_local = colIdx[i];
				
			
			// If not the diagonal (this is set separately, ignored if found in the arrays
			if ( C_local != rowCounter ) {
				// Add the column index
				A.C.Add( C_local );
				// Add the value
				//std::cout << "Adding V" << std::endl;
				A.V.Add( values[i] );
				// Set the rowPtr for the next row to begin at the end of the A.C/A.V vector 
				//std::cout << "Setting Row" << std::endl;
				A.R[ rowCounter + 1 ] = A.C.Size();
			}
		}
		rowCounter++;
	}
}

template<typename T>
void Matrix<T>::AddRow( T diag, std::vector<int> colIdx, std::vector<T> values )
{
	int nz = colIdx.size();
	if (values.size() != nz) std::cerr << "You have passed arrays of different lengths as ColIdx and Values to Matrix<T>::AddRow()" << std::endl;

	AddRow(diag, nz, &colIdx[0], &values[0]);
	
}
template<typename T>
void Matrix<T>::AddLocalRow( T diag, std::vector<int> colIdx, std::vector<T> values )
{
	int nz = colIdx.size();
	if (values.size() != nz) std::cerr << "You have passed arrays of different lengths as ColIdx and Values to Matrix<T>::AddRow()" << std::endl;

	AddLocalRow(diag, nz, &colIdx[0], &values[0]);
	
}

template<typename T>
void Matrix<T>::Complete()
{
	// In the case that we added a row with no A or B elements, some rowPointers will not be updated
	// For example, we might get left with { 0, 2, 3, 4, -1, 5, 6 } in A.R or B.R
	// Note that the vector was initialized as { 0, -1, -1, -1, ... , -1 }
	// TODO: parallelize? tricky.
	for (int i = 1; i < A.R.Size(); ++i) {
		if (A.R[i] < 0)
			A.R[i] = A.R[i-1];
		if (B.R[i] < 0)
			B.R[i] = B.R[i-1];
    }
	// Now we have { 0, 2, 3, 4, 4, 5, 6 }
	
	// TODO: Add some checks that all rows have actually been added, and no more.
	
	// Complete the parallelContext initialization.
	// Now that the structure of the matrix is known, the vector scatter patterns can be computed.
	parallelContext->ComputeScatter(this);
	A.C.Optimize();
	A.V.Optimize();
	B.C.Optimize();
	B.V.Optimize();
}



/*template <typename T> Matrix<T>* Matrix<T>::SerialFromArrays(int size, int nz, int* rowPtr, int* colIdx, T* values)
{
    auto* m = new Matrix<T>();col
    m->size = size;
    m->A.N = nz;
    m->A.R = Vector<int>::SerialFromArray(size, rowPtr);
    m->A.C = Vector<int>::SerialFromArray(nz, colIdx);
    m->A.V = Vector<T>::SerialFromArray(nz, values);
    m->isMPI = false;
    return m;
}

template <typename T>
Matrix<T>* ParallelFromArrays(int nRows,
                              int nz1,
                              int nz2,
                              int* rowPtr1,
                              int* rowPtr2,
                              int* colIdx1,
                              int* colIdx2,
                              T* values1,
                              T* values2)
{*/
    /*auto *m = new Matrix<T>();
    m->rows = nRows;
    m->local.N = nz1;
    m->local.R = Vector<int>::SerialFromArray(nRows, rowPtr1);
    m->local.C = Vector<int>::SerialFromArray(nz1, 	 colIdx1);
    m->local.V = Vector<T>  ::SerialFromArray(nz1,   values1);
    m->remote.R = Vector<int>::SerialFromArray(nRows, rowPtr2);
    m->remote.C = Vector<int>::SerialFromArray(nz2,   colIdx2);
    m->remote.V = Vector<T>  ::SerialFromArray(nz2,   values2);
    m->isMPI = true;

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
MPI_Comm_size(MPI_COMM_WORLD, &size);
int bottom = round(((double)globalrows) * (double)rank / (double)size);
int top = round(((double)globalrows) * ((double)rank+1.0) / (double)size) - 1;
    m->procStart = bottom; m->procEnd = top;

    m->scatter = m->ComputeMPIScatter();
    return m;*/
//}

/*template <typename T> Matrix<T>* Matrix<T>::SerialFromFile(std::string fileName)
{
    auto* m = new Matrix<T>();

    std::ifstream infile(fileName);
    int nz;
    infile >> m->size >> nz >> nz;
    m->A.N = nz - m->size; // remove diagonal
    m->Allocate();
    m->A.R->at(0) = 0;
    int idx = 0;
    for (int i = 0; i < nz; ++i) {
	int r, c;
	T v;
	infile >> r >> c >> v;
	// std::cout << r << c << v << std::endl;
	if (r != c) {
	    m->A.R->at(r) = idx + 1;
	    m->A.C->at(idx) = c - 1;
	    m->A.V->at(idx) = v;
	    idx++;
	} else {
	    m->D->at(c - 1) = v;
	}
    }
    m->A.R->at(m->size) = m->A.N;
    infile.close();
    return m;
}

template <typename T> Matrix<T>* Matrix<T>::ParallelFromFile(std::string fileName)
{
    std::ifstream infile(fileName);
    int nz, globalrows, rank = 0, size = 1;
    infile >> globalrows >> nz >> nz;
#ifdef CHAOS_MPI_FOUND
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
#endif
    infile.close();
    int bottom = round(((double)globalrows) * (double)rank / (double)size);
    int top = round(((double)globalrows) * ((double)rank + 1.0) / (double)size) - 1;
    return ParallelFromFile(fileName, bottom, top);
}*/

template <typename T>
Matrix<T>::Matrix(std::string fileName, int NZperRowHint)
{
    std::ifstream infile(fileName);
    int nz, ncols, globalrows;
    infile >> globalrows >> ncols >> nz;
	
	parallelContext = new ParallelContext(globalrows);
	if (parallelContext->MPIsize > 0) isParallel = true;
	rowCounter = 0;

	// Resize the vector which holds the diagonal
	D.Resize(parallelContext->size);
	
	// Resize the local matrix (A), local memory for NZ cannot be reserved easily.
	A.R.Resize( Size() + 1 );
	A.R.SetAll(-1);
	A.R[0] = 0;
	A.C.Reserve(NZperRowHint * Size());
	A.V.Reserve(NZperRowHint * Size());
	
	if (isParallel) {
		// ... and then the off-process matrix (B)
		B.R.Resize( Size() + 1 );
		B.R.SetAll(-1);
		B.R[0] = 0;
		B.C.Reserve(NZperRowHint * Size()/10); // Assuming approx 10% off-process
		B.V.Reserve(NZperRowHint * Size()/10);
	}

	int r_last = parallelContext->rangeBottom;
	std::vector<int> colIdx;
	std::vector<T> values;
	T diag;
	
    for (int i = 0; i < nz; ++i) {
		int r, c; T v;
		infile >> r >> c >> v;
		//std::cout << r << " " << c << " " << v << std::endl;
		// Correct 1-based to 0-based
		r--; c--;
			
		if (r >= parallelContext->rangeBottom && r <= parallelContext->rangeTop)
		{
			if (r != r_last) { // New row
				AddRow( diag, colIdx, values );
				colIdx.clear();
				values.clear();
				diag = 0.0;
				r_last = r;
			}
			// Add to current row
			if ( r == c ) { // Diagonal
				diag = v;
			} else { // Offdiagonal
				colIdx.push_back(c);
				values.push_back(v);
			}
		}
	}
	AddRow( diag, colIdx, values ); // Add the final row
	infile.close();
	Complete();
}




template <typename T>
T Matrix<T>::at(int row, int col)
{
    for (int i = A.R[row]; i < A.R[row + 1]; i++)
		if (A.C[i] == col)
			return A.V[i];
	for (int i = B.R[row]; i < B.R[row + 1]; i++)
		if (B.C[i] == col)
			return B.V[i];
    return 0.0;
}

template<typename T>
void Matrix<T>::Multiply(Vector<T> * mult, Vector<T> * result)
{
	for (int idx = 0; idx < Size(); idx++) {
		double rowSum = 0.0;
		for (int j = A.R[idx]; j < A.R[idx+1]; j++) {
			rowSum += A.V[j] * mult->A[ A.C[j] ];
		}
		for (int j = B.R[idx]; j < B.R[idx+1]; j++ ) {
			rowSum += B.V[j] * mult->B[ B.C[j] ];
		}
		rowSum += D[idx] * mult->A[idx];
		result->at(idx) = rowSum;
	}
	return;
}



template class Matrix<double>;
template class Matrix<int>;
