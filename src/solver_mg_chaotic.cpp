#include "solver.h"

/// Smooth a single row of Ax=b.
void Solver::RelaxRow(Matrix<double> * mat, Vector<double> * x, Vector<double> * rhs, int row)
{
	double rowSum = 0;
	for (int j = mat->A.R[row]; j < mat->A.R[row+1]; j++) {
		rowSum += mat->A.V[j] * x->A[ mat->A.C[j] ];
	}
	for (int j = mat->B.R[row]; j < mat->B.R[row+1]; j++ ) {
		rowSum += mat->B.V[j] * x->B [ mat->B.C[j] ];
	}
	x->A[row] = ( rhs->A[row] - rowSum ) / mat->D[row];
}
/// Smooth a single row of Ax=b. Ignores the (B) portion of the matrix which corresponds to off-process elements.
void Solver::RelaxRowNoComm(Matrix<double> * mat, Vector<double> * x, Vector<double> * rhs, int row) {
	double rowSum = 0;
	for (int j = mat->A.R[row]; j < mat->A.R[row+1]; j++) {
		rowSum += mat->A.V[j] * x->A[ mat->A.C[j] ];
	}
	x->A[row] = ( rhs->A[row] - rowSum ) / mat->D[row];
}

/// Smoothes rows of Ax=b on a given level, using a gauss-seidel-like method, from row start to row end. The off-process matrix (B) is ignored. Useful for estimaating a solution before communications have finished,
/// or using alone in a non-communicating algorithm. Note that ignoring communication entirely will result in a poor solution.
void Solver::SmoothNoComm(Matrix<double> * mat, Vector<double> *x, Vector<double> *rhs, int start, int end)
{
	double rowSum;
	for (int row = start; row < end; row++) {
		rowSum = 0;
		for (int j = mat->A.R[row]; j < mat->A.R[row+1]; j++) {
			rowSum += mat->A.V[j] * x->A[ mat->A.C[j] ];
		}
		x->A[row] = ( rhs->A[row] - rowSum ) / mat->D[row];
	}
}


/// Smoothes rows of Ax=b on a given level, using a gauss-seidel-like method, from row start to row end.
void Solver::Smooth(int level, int start, int end)
{
	double rowSum;
	if (level == 0) {
		for (int row = start; row < end; row++) {
			rowSum = 0;
			for (int j = mats[level]->A.R[row]; j < mats[level]->A.R[row+1]; j++) {
				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
			}
			for (int j = mats[level]->B.R[row]; j < mats[level]->B.R[row+1]; j++ ) {
				rowSum += mats[level]->B.V[j] * xs[level]->B [ mats[level]->B.C[j] ];
			}
			xs[level]->A[row] = ( bs[level]->A[row] - rowSum ) / mats[level]->D[row];
		}
	} else {
		for (int row = start; row < end; row++) {
			rowSum = 0;
			for (int j = mats[level]->A.R[row]; j < mats[level]->A.R[row+1]; j++) {
				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
			}
			for (int fi = 0; fi < cfMaps[level][0][row].size(); fi++) {
				int f_idx = cfMaps[level][0][row][fi];
				for (int fj = mats[0]->B.R[  f_idx  ]; fj < mats[0]->B.R[f_idx+1]; fj++ ) {
					rowSum += mats[0]->B.V[fj] * xs[level]->B [ mats[0]->B.C[fj] ];
				}
			}
			xs[level]->A[row] = ( bs[level]->A[row] - rowSum ) / mats[level]->D[row];
		}
	}
}

/// This is an alternative Smooth function, which will iterate over a matrix using the partitioning of the coarsest matrix and the coarse-to-fine mapping, rather than work-sharing directly on the fine matrix.
/// This has an extra level of indirection and is inefficient, but can be used for alternate algorithms, particularly where multiple levels are performed asynchronsouly and it is necessary that only fine indexes
/// belonging to a coarse partitioning are iterated on.
// void Solver::SmoothWithCoarsePartitioning(int level)
// {
// 	double rowSum;
// 	if (level == 0) {
// 		for (int row = start; row < end; row++) {
// 			rowSum = 0;
// 			for (int j = mats[level]->A.R[row]; j < mats[level]->A.R[row+1]; j++) {
// 				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
// 			}
// 			for (int j = mats[level]->B.R[row]; j < mats[level]->B.R[row+1]; j++ ) {
// 				rowSum += mats[level]->B.V[j] * xs[level]->B [ mats[level]->B.C[j] ];
// 			}
// 			x->A[row] = ( bs[level]->A[row] - rowSum ) / mats[level]->D[row];
// 		}
// 	} else {
// 		for (int row = start; row < end; row++) {
// 			rowSum = 0;
// 			for (int j = mats[level]->A.R[row]; j < mats[level]->A.R[row+1]; j++) {
// 				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
// 			}
// 			for (int fi = 0; fi < cfMaps[level][0][level].size(); fi++) {
// 				int f_idx = cfMaps[level][0][level][fi];
// 				for (int fj = mats[0]->B.R[  f_idx  ]; fj < mats[0]->B.R[f_idx+1]; fj++ ) {
// 					rowSum += mats[0]->B.V[fj] * xs[level]->B [ mats[0]->B.C[fj] ];
// 				}
// 			}
// 			x->A[row] = ( bs[level]->A[row] - rowSum ) / mats[level]->D[row];
// 		}
// 	}
// }


/* These three functions provide a barrier that "flips" so that it can be reused over and over.
 * The internal counter for the barrier will only be incremented if the calling thread is
 * behind all other threads. In this way, only meaningful iterations of the chaotic method will count
 * towards their iteration count.
 * 
 * barrier_flip is the current barrier number (0 - 4)
 * thread_its is a vector of iteration-counts, of size num_threads*4
 * thread is the index of the calling thread
 * 
 * While flipping through the 4 states of the barrier, the other states are reset so that the barrier can be reused
 */

/// Increment the thread counter if this thread has performed the same or fewer iterations than the other threads
int Solver::IncrementCounterIfCounts(std::vector<int> & thread_its, int & thread, int & barrier_flip) {
	
	int offset = thread_its.size()/4 * barrier_flip;
	int next_offset = thread_its.size()/4 * ((barrier_flip + 2)%4);

	// Reset the next barrier
	thread_its[thread + next_offset] = 0;
	
	for (int i = 0; i < thread_its.size()/4; i++)
		if (i != thread)
			if (thread_its[i+offset] < thread_its[thread+offset])
				return thread_its[thread+offset];
	++thread_its[thread+offset];
	return thread_its[thread+offset];
}

/// Check whether this thread has performed the same or fewer iterations than the other threads
bool Solver::DoesCount(std::vector<int> & thread_its, int & thread, int & barrier_flip) {
	int offset = thread_its.size()/4 * barrier_flip;
	int next_offset = thread_its.size()/4 * ((barrier_flip + 2)%4);
	// Reset the next barrier
	thread_its[thread + next_offset] = 0;
	
	for (int i = 0; i < thread_its.size()/4; i++)
		if (i != thread)
			if (thread_its[i+offset] < thread_its[thread+offset])
				return false;
	return true;
}

/// Increment the counter regardless of the iteration-count of other threads
int Solver::IncrementCounter(std::vector<int> & thread_its, int & thread, int & barrier_flip) {
	int offset = thread_its.size()/4 * barrier_flip;
	int next_offset = thread_its.size()/4 * ((barrier_flip + 2)%4);
	// Reset the next barrier
	thread_its[thread + next_offset] = 0;
	++thread_its[thread+offset];
	return thread_its[thread+offset];
}

/// The prinicple chaotic multigrid routine.
bool Solver::ChaoticMGSolve() {

	// Revert to normal chaotic method if numLevels = 1
	if (numLevels == 1) {
		Chaos::Log("Reverting to standard Chaotic method (no multigrid), only one level exists.", 2);
		return ChaoticSolve();
	}


	bool stop = false;
	bool ready = false;

	// Counts the number of iterations performed by each thread on each level
	std::vector<int> thread_its;
	thread_its.resize(omp_get_max_threads()*4);
	for (int i = 0; i < thread_its.size(); i++) thread_its[i] = 0;

	// Thread barrier for all threads (comp + comm)
	bar_t barrier(omp_get_max_threads());
	bar_t barrier_comp(omp_get_max_threads()-1);

	#pragma omp parallel
	{
		int id; // for barrier tracking
		int thread = omp_get_thread_num();
		int num_comp_threads = omp_get_num_threads() -1;
		int num_threads = omp_get_num_threads();
		int barrier_flip = 0;
		int k;
		bool bar_sent = false;

		if (num_threads == 1)
			Chaos::Log("WARNING: Chaotic method invoked with 1 thread.", 0);

		// COMPUTATION THREADS
		if (thread+1 != num_threads) // The last thread becomes the comm thread
		{
			// Get the worksharing range (bottom -> top) for each level
			std::vector<int> bottom, top;
	    	for (int i = 0; i < numLevels; i++) {
	    		bottom.push_back ( (mats[i]->Size() * ((double)thread/(double)num_comp_threads)) );
				top.push_back    ( (mats[i]->Size() * ((double)(thread+1)/(double)num_comp_threads)) );
			}
			barrier.Wait(); // Begin cycle
			
			while (!stop) {

				/* Finest level pre-smoothing */
				while (true) {
					Smooth(0, bottom[0], top[0]);
					int k = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
					if (k == preSmoothIterations && bar_sent == false) { id = barrier.BeginWait(); bar_sent = true;}
					if (k >= preSmoothIterations && bar_sent && barrier.EndWait(id)) {
						barrier_flip = (barrier_flip +1 ) % 4;
						bar_sent = false;
						break;
					}
				}

				/* Perform the sawtooth restriction through all levels, no synchronization necessary */
				ChaoticSawtoothRestrict(bottom[numLevels-1], top[numLevels-1], barrier_comp);

				// Thread-barrier is necessary here to make sure all coarse-grid equation systems are correctly initialized
				barrier.Wait();

				/* Prolong and smooth the coarse grids */
				int level = numLevels-1;
				while (level > 0) {
					while (true) {
						Smooth(level, bottom[level], top[level]);
						int k = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
						if (k == postSmoothIterations && bar_sent == false) { id = barrier.BeginWait(); bar_sent = true;}
						if (k >= postSmoothIterations && bar_sent && barrier.EndWait(id)) {

							// Prolong
							for (int i = bottom[level]; i < top[level]; i++) {
								for (int k = 0; k < cfMaps[level][level-s][i].size(); k++)
				 				{
				 					// Fine index
				 					int idx2 = cfMaps[level][level-s][i][k];		
								 	// Take x (coarse) and correct x (fine)
								 	xs[level-s]->at(idx2) += xs[level]->at(i);
			 	 				}
		 	 				}

		 	 				level -= s;
		 	 				barrier_flip = (barrier_flip +1 ) % 4;
		 	 				bar_sent = false;
							break; // Proceed to next level
						}
					}
				}

				/* Finest level post-smoothing */
				while (true) {
					Smooth(0, bottom[0], top[0]);
					int k = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
					if (k == postSmoothIterations && bar_sent == false) { id = barrier.BeginWait(); bar_sent = true;}
					if (k >= postSmoothIterations && bar_sent && barrier.EndWait(id)) {
						barrier_flip = (barrier_flip +1 ) % 4;
						bar_sent = false;
						break;
					}
				}

				#pragma omp flush (stop)
			}

		// COMMUNICATION THREAD
		} else {
			// The communication thread is in charge of scheduling the computation threads. It tells them when to stop and also which level they should be working on.
			#ifdef CHAOS_MPI_FOUND
				MPI_Request request;
			#endif
			bool MPI_barrier_sent = false;
			int MPI_barrier_result = 0;

			barrier.Wait(); // Begin cycle

			while(!stop) {

				/* Communicate the fine grid (pre) */
				int k = 0;
				while(true){
					// Only communicate when it "counts"
					//int new_k = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
					if(!DoesCount(thread_its, thread, barrier_flip)) continue;
					if (k < preSmoothIterations) {
						xs[0]->ScatterBegin();
						xs[0]->ScatterEnd();
					}
					k = IncrementCounter(thread_its, thread, barrier_flip);

					// Tell other MPI processes that we are ready, and then start checking if they are also ready
					if (k == preSmoothIterations) {
						#ifdef CHAOS_MPI_FOUND
							MPI_Ibarrier(MPI_COMM_WORLD, &request);
						#endif
			            MPI_barrier_sent = true;
					}
					if (k >= preSmoothIterations) {
						#ifdef CHAOS_MPI_FOUND
							MPI_Test(&request, &MPI_barrier_result, MPI_STATUS_IGNORE);
						#else
							MPI_barrier_sent = 1;
						#endif
						// All MPI processes are ready, break the loop and inform the threads that we are done.
						if (MPI_barrier_result == 1) {
				            id = barrier.BeginWait();
				            // While waiting for other MPI processes, and for our computation threads to stop, this is a good opportunity to begin the residual check
				            BeginComputeResidual();
				            #ifdef CHAOS_MPI_FOUND
								MPI_Wait(&request, MPI_STATUS_IGNORE);
							#endif
				            
				            while (!barrier.EndWait(id)) {
				            	int m = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
				        	}
				            barrier_flip = (barrier_flip +1 ) % 4;
				            MPI_barrier_sent = false;
				            MPI_barrier_result = 0;

							break;
						}
					}

				}

				/* While the computation threads perform restriction, loop through the parallel portions of the X vectors and reset them to zero */
				for (int i = 1; i < numLevels; i++) {
					std::fill(xs[i]->B.begin(), xs[i]->B.end(), 0.0);
				}

				// Thread-barrier is necessary here to make sure all coarse-grid equation systems are correctly initialized
				barrier.Wait();			

				int level = numLevels-1;
				while (level > 0) {
					k = 0;
					while(true){
						//int new_k = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
						if(!DoesCount(thread_its, thread, barrier_flip)) continue;
						if (k < postSmoothIterations) {
							xs[0]->ScatterBeginProxy( level, xs[level]->A, fcMaps );
							xs[0]->ScatterEndProxy  ( level, xs[level]->B );
						}
						k = IncrementCounter(thread_its, thread, barrier_flip);

						// Tell other MPI processes that we are ready, and then start checking if they are also ready
						if (k == postSmoothIterations) {
							#ifdef CHAOS_MPI_FOUND
								MPI_Ibarrier(MPI_COMM_WORLD, &request);
							#endif
				            MPI_barrier_sent = true;
						} 
						if (k >= postSmoothIterations) {
							#ifdef CHAOS_MPI_FOUND
								MPI_Test(&request, &MPI_barrier_result, MPI_STATUS_IGNORE);
							#else
								MPI_barrier_result = 1;
							#endif
							// All MPI processes are ready, break the loop and inform the threads that we are done.
							if (MPI_barrier_result == true) {
								// Wait for MPI processes first
								#ifdef CHAOS_MPI_FOUND
									MPI_Wait(&request, MPI_STATUS_IGNORE);
								#endif
					            id = barrier.BeginWait();
					            while (!barrier.EndWait(id)) IncrementCounterIfCounts(thread_its, thread, barrier_flip);
					            barrier_flip = (barrier_flip +1 ) % 4;
					            MPI_barrier_sent = false;
					            MPI_barrier_result = false;
								level -=s;
								break;
							}
						}
					}

				}

				/* Communicate the fine grid (post) */
				k = 0;
				while(true){
					//int new_k = IncrementCounterIfCounts(thread_its, thread, barrier_flip);
					if(!DoesCount(thread_its, thread, barrier_flip)) continue;
					if (k < postSmoothIterations) {
						xs[0]->ScatterBegin();
						xs[0]->ScatterEnd();
					}
					k = IncrementCounter(thread_its, thread, barrier_flip);

					// Tell other MPI processes that we are ready, and then start checking if they are also ready
					if (k == postSmoothIterations) {
						#ifdef CHAOS_MPI_FOUND
							MPI_Ibarrier(MPI_COMM_WORLD, &request);
						#endif
			            MPI_barrier_sent = true;
					}
					if (k >= postSmoothIterations) {
						#ifdef CHAOS_MPI_FOUND
							MPI_Test(&request, &MPI_barrier_result, MPI_STATUS_IGNORE);
						#else
							MPI_barrier_result = 1;
						#endif

						// All MPI processes are ready, break the loop and inform the threads that we are done.
						if (MPI_barrier_result == true) {
				            // Grab the residual and check if the stop criterion are reached, before the thread barrier.
				            // This is cheap, since the residual is actually computed much earlier (before the last cycle)
				            norm = EndComputeResidual();
			 		 		Chaos::Log("Iteration " + std::to_string((long long)iterations) +": " + std::to_string((long double)norm), 3);
			 		 		iterations++;
							if (IsConverged()) stop = true;
			 		 		if (IsDiverged()) stop = true;
			 		 		#pragma omp flush (stop)

				            id = barrier.BeginWait();
				            #ifdef CHAOS_MPI_FOUND
								MPI_Wait(&request, MPI_STATUS_IGNORE);
							#endif
				            while (!barrier.EndWait(id)) IncrementCounterIfCounts(thread_its, thread, barrier_flip);
				            barrier_flip = (barrier_flip +1 ) % 4;
				            MPI_barrier_sent = false;
				            MPI_barrier_result = false;
							break;
						}
					}
				}
			}
		}
	}
}

/// Performs full sawtooth restriction from finest to coarsest grid between two indices on the coarse grid.
void Solver::ChaoticSawtoothRestrict(int bottom_coarse, int top_coarse, bar_t &barrier_comp) {
	
	//barrier_comp.Wait();
	
	if (numLevels - 1 > s) {
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			// ... Loop through all the indices in mesh S (first coarse level)
			for (int j = 0; j < cfMaps[numLevels-1][s][i].size(); j++) {
				int idx_s = cfMaps[numLevels-1][s][i][j];
				bs[s]->at(idx_s) = 0; // Initialize to zero
				xs[s]->at(idx_s) = 0; // reset solution vector to zero
			}
		}
		//barrier_comp.Wait();
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			// ... Loop through all the indices in mesh S (first coarse level)
			for (int j = 0; j < cfMaps[numLevels-1][s][i].size(); j++) {

				// The index in s
				int idx_s = cfMaps[numLevels-1][s][i][j];

				// Now get the map from that index to the finest mesh
				for (int k = 0; k < cfMaps[s][0][idx_s].size(); k++)
				{
					// The index in finest mesh
					int idx2 = cfMaps[s][0][idx_s][k];			
					// Compute residual (b-Ax) and sum into b
					bs[s]->at(idx_s) += ComputeRowResidual(idx2, 0);
				}
			}
		}
	} else if (numLevels - 1 == s) {
		// Special case where there are only two levels. Restrict the finest mesh into the coarsest mesh.
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			
			// Set vectors
			bs[1]->at(i) = 0; // Initialize to zero
			xs[1]->at(i) = 0; // Also reset solution vector to zero
		}
		//barrier_comp.Wait();
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			// Now get the map from that index to the finest mesh
			for (int k = 0; k < cfMaps[1][0][i].size(); k++)
			{
				// The index in finest mesh
				int idx2 = cfMaps[1][0][i][k];			
				// Compute residual (b-Ax) and sum into b
				bs[s]->at(i) += ComputeRowResidual(idx2, 0);
			}
		}
	}
	/* 
	 * Now, restrict into the coarser levels, using the fact that all xs[] are zero and so the residual
	 * on the fine grid (b-Ax) from ComputeRowResidual can be replaced with just b (Ax=0).
	 */
	
	// Loop downwards, towards the coarsest mesh
	for (int L = s; L+s < numLevels - 1; L+=s)
	{
		// ... Loop through the coarsest level
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			// ... Loop through all the indices in mesh S+L (next coarse level)
			for (int j = 0; j < cfMaps[numLevels-1][s+L][i].size(); j++) {
				// The index in s+L
				int idx_sL = cfMaps[numLevels-1][s+L][i][j];
				
				// Set the vectors to zero
				xs[L+s]->at(idx_sL) = 0.0;
				bs[L+s]->at(idx_sL) = 0.0;
			}
		}
		//barrier_comp.Wait();
		// ... Loop through the coarsest level
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			// ... Loop through all the indices in mesh S+L (next coarse level)
			for (int j = 0; j < cfMaps[numLevels-1][s+L][i].size(); j++) {
				// The index in s+L
				int idx_sL = cfMaps[numLevels-1][s+L][i][j];

				// Now get the map from that index to the finer mesh
				for (int k = 0; k < cfMaps[s+L][L][idx_sL].size(); k++)
				{
					int idx2 = cfMaps[s+L][L][idx_sL][k];			
					// Take b (fine) and sum into b (coarse)
					bs[L+s]->at(idx_sL) += bs[L]->at(idx2);
				}
			}
		}
	}

	// Finally, the coarsest level.
	// ... Loop through the coarsest level
	if (numLevels - 1 > s) { // only do if the first level isn't the coarsest level
		for (int i = bottom_coarse; i < top_coarse; ++i) {
						
			// Set the vectors to zero
			xs[numLevels-1]->at(i) = 0.0;
			bs[numLevels-1]->at(i) = 0.0;
		}
		//barrier_comp.Wait();
		for (int i = bottom_coarse; i < top_coarse; ++i) {
			// Now get the map from that coarse level to the finer mesh
			for (int k = 0; k < cfMaps[numLevels-1][numLevels-1-s][i].size(); k++)
				{
					int idx2 = cfMaps[numLevels-1][numLevels-1-s][i][k];			
					// Take b (fine) and sum into b (coarse)
					bs[numLevels-1]->at(i) += bs[numLevels-1-s]->at(idx2);
				}
		}
	}
}
