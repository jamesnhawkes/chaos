#include "solver.h"

#include <chrono>


void Solver::Reset( ) {

	rtol = 0.0;
	rdtol = 1e300;
	atol = 0.0;
	adtol = 1e300;
	maxIterations = 1000000;

	solverType = SolverType::MG_V;
	strongCouplingCoefficient = 0.25;

	currentLevel = 0;
	numLevels = 1;
	maxLevels = 999;
	preSmoothIterations = 2;
	postSmoothIterations = 4;
	coarsestSmoothIterations = 4;
	minimumGridSize = 100;
	minimumGridSizeRelative = 0.0;
	iterations = 0;
	aggregationFactor = 4;
	s = 1;
}



double Solver::ComputeResidual(bool doScatter) {
	if (doScatter) x->Scatter();
	//#pragma omp for
	for (int i = 0; i < A->Size(); i++) {
		r->A[i] = ComputeRowResidual(i);
	}
	return r->Norm();
}
void Solver::BeginComputeResidual(bool doScatter) {
	if (doScatter) x->Scatter();
	//#pragma omp for
	for (int i = 0; i < A->Size(); i++) {
		r->A[i] = ComputeRowResidual(i);
	}
	return r->NormBegin();
}
double Solver::EndComputeResidual() {
	return r->NormEnd();
}

void Solver::SetMatrix( Matrix<double> * A, bool reuseAggregation )
{
	this->A = A;

	if (!reuseAggregation)
		PerformAggregation();
	else
		FillCoarseGridsUsingPreviousAggregation();

	return;
}

void Solver::PerformAggregation() 
{
	Chaos::StartTimer();

	numLevels = 1;

	// Delete old matrices and supporting vectors
	for (int i = 1; i < mats.size(); i++) {
		delete( mats[i] );
		delete( xs[i] );
		delete( rs[i] );
		delete( bs[i] );
		delete( work1[i] );
	}
	if (work1.size() > 0) delete work1[0];

	mats.clear();
	xs.clear();
	rs.clear();
	bs.clear();
	work1.clear();
	cfMaps.clear();

	// Add fine grid to the vectors containing all the matrices, x's, b's and r's
	mats.push_back(this->A);
	xs.push_back( this->x );
	bs.push_back( this->b );
	rs.push_back( this->r );
	work1.push_back ( new Vector<double>(1,0.0));

	// Add the 0th index to this 4D map. cfMaps maps coarse points to fine points,
	// Since there are no finer meshes, cfMaps[0] should contain nothing.
	cfMaps.clear(); fcMaps.clear();
	cfMaps.push_back( std::vector<std::vector<std::vector<int>>>() );
	fcMaps.push_back( std::vector<std::vector<int>>() );
	//fcMaps[0].push_back(std::vector<int>());


	for (int i = 0; i < maxLevels-1; i++)
	{
		Matrix<double> * cMat = DoAggregation2(mats[i]);
		if ( cMat->Size() < minimumGridSize ) {
			delete (cMat);
			break;
		}
		if ( cMat->Size()/A->Size() < minimumGridSizeRelative ) {
			delete (cMat);
			break;
		}
		if ( cMat->Size() == mats[i]->Size()) {
			Chaos::Log("WARNING: Aggregation could not coarsen beyond size " + std::to_string((long long)cMat->Size()), 2, true);
			delete (cMat);
			break;
		}
		
		mats.push_back ( cMat );
		xs.push_back( new Vector<double>( cMat, 0.0) );

		// Manually set the parallel context of this vector and resize the buffers to allow proxy scattering
		xs[i+1]->parallelContext = xs[i]->parallelContext;
		xs[i+1]->B.resize(xs[i]->B.size());
		xs[i+1]->sendbuf.resize( xs[i]->sendbuf.size() );


		bs.push_back( new Vector<double>( cMat, 0.0) );
		rs.push_back( new Vector<double>( cMat, 0.0) );
		work1.push_back( new Vector<double>(1,0.0) );
		numLevels++;

		// double sum = 0.0;
		// for (int k = 0; k < cMat->A.V.A.size(); k++)
		// 	sum += cMat->A.V.A[k];
		// for (int k = 0; k < cMat->Size(); k++)
		// 	sum += cMat->D[k];
		// for (int k = 0; k < mats[0]->B.V.A.size(); k++)
		//  	sum += mats[0]->B.V.A[k];
		// if (Chaos::GetRank() == 4)
		// 	std::cout << cMat->Size() << " (" << sum << ")" << std::endl;


	}

	int numLevelsMinAllProc = numLevels;
	#ifdef CHAOS_MPI_FOUND
		MPI_Allreduce( &numLevels, &numLevelsMinAllProc, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
	#endif
	numLevels = numLevelsMinAllProc;

	Chaos::StopTimer();
	Chaos::Log("Aggregation took " + Chaos::GetTimerMillisecondsString() + "ms (" + std::to_string((long long)numLevels-1) + " levels created).", 1);


}

void Solver::FillCoarseGridsUsingPreviousAggregation()
{
	Chaos::StartTimer();


	for (int level = 1; level < numLevels; level++)
	{
		int c = level; // coarse index
		int f = level -1; // fine index

		#pragma omp parallel for
		for (int i = 0; i < mats[c]->Size(); i++)
		{
			// Reset diagonal
			mats[c]->D[i] = 0.0;

			// Reset off-diagonal
			for (int j = mats[c]->A.R[i]; j < mats[c]->A.R[i+1]; j++)
				mats[c]->A.V[j] = 0;
		}

		#pragma omp parallel for
		for (int i = 0; i < mats[c]->Size(); i++)
		{
			// Loop over fine elements which contribute to this coarse element
			for (int k = 0; k < cfMaps[c][f][i].size(); k++)
			{
				int idx = cfMaps[c][f][i][k];
		
				// Sum the diagonal
				mats[c]->D[i] += mats[f]->D[idx];

				// The offdiagonal of each fine row
				for (int kf = mats[f]->A.R[idx]; kf < mats[f]->A.R[idx+1]; kf++ ) {

					// for each fine column, find the coarse element it belongs to
					int col = mats[f]->A.C[kf];
					int coarse_col = fcMaps[c][f][col];

					// if the colIdx is THIS element, add it to the diagonal
					if (coarse_col == i) {
						mats[c]->D[i] += mats[f]->A.V[kf];
					} else {
					// if the colIdx already exists in the colIdxs vector, add the value to that index
						auto it = std::find (mats[c]->A.C.A.begin() + mats[c]->A.R[i], mats[c]->A.C.A.begin() + mats[c]->A.R[i+1], coarse_col);
						if (it != mats[c]->A.C.A.begin() + mats[c]->A.R[i+1]) {
							// It already exists
							int pos = it - mats[c]->A.C.A.begin();
							mats[c]->A.V[pos] += mats[f]->A.V[kf];
						} else {
						// else add it to the colIdx vector
							Chaos::Log("Trying to fill using a previous aggregation, but the matrix structure has changed.", 0);
						}

					}
				}

			}

		}
	}


	// Reset the xs, bs, and rs on the coarser levels
	for (int i = 1; i < numLevels; i++)
	{
		xs[i]->SetAll(0.0);
		bs[i]->SetAll(0.0);
		rs[i]->SetAll(0.0);
	}

	Chaos::StopTimer();
	Chaos::Log("Re-filling previous aggregation with new matrix took " + Chaos::GetTimerMillisecondsString() + "ms (" + std::to_string((long long)numLevels) + " levels filled).", 1);

}

void Solver::ComputeSolverLevels()
{
	coarsestSolverLevel = ((numLevels-1)/s)*s;
	numSolverLevels = coarsestSolverLevel/s + 1;

}

bool Solver::Solve()
{

	//Chaos::StartTimer();
	
	x->ScatterBegin();
	x->ScatterEnd();
	ComputeSolverLevels();
	norm0 = ComputeResidual();
	iterations = 0;
	Chaos::Log("Iteration 0: " + std::to_string((long double)norm0), 3);
	bool success;

	if (solverType == SolverType::MG_V || solverType == SolverType::MG_W || solverType == SolverType::MG_F || solverType == SolverType::MG_Sawtooth )
	{
		success = ClassicalMGSolve();
	}
	else if (solverType == SolverType::MG_Chaotic)
	{
		success = ChaoticMGSolve();
	} else if (solverType == SolverType::Chaotic) {
		success = ChaoticSolve();
	}

	norm = ComputeResidual();

	//Chaos::StopTimer();
	// if (success) {
	// 	Chaos::Log("Converged in " + std::to_string((long long)iterations) + " iterations ("+Chaos::GetTimerMillisecondsString() + "ms)" , 1);
	// 	Chaos::Log("Final relative convergence: " + std::to_string((long double)norm/norm0), 1 );
	// } else {
	// 	Chaos::Log("DIVERGED in " + std::to_string((long long)iterations) + " iterations ("+Chaos::GetTimerMillisecondsString() + "ms)" , 1);
	// 	Chaos::Log("Final relative convergence: " + std::to_string((long double)norm/norm0), 1 );
	// }
	return success;

}

bool Solver::IsConverged() {

	if (norm/norm0 <= rtol)
		return true;
	if (norm <= atol)
		return true;
	return false;
}

bool Solver::IsDiverged() {
	if (norm/norm0 >= rdtol)
		return true;
	if (norm >= adtol)
		return true;
	return false;
}