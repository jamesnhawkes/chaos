#ifndef __CHAOS_DEFINES_H__
#define __CHAOS_DEFINES_H__


#cmakedefine CHAOS_MPI_FOUND
#ifdef CHAOS_MPI_FOUND
	#include "mpi.h"
#endif

#cmakedefine CHAOS_DEBUG

#include <string>
#include <iostream>
#include <chrono>

class Chaos
{
	// Controls parallel layout and initialization, parsing of argc/argv, etc.
	// Spawns solvers, may provide interfaces to matrices/vectors
public:

	Chaos(int argc, char *argv[])
	{
		// 0 - Messages will always be printed, never recommended.
		// 1 - Summary messages, used to print convergence/divergence, elapsed time and number of iterations.
		// 2 - More detailed information about the solver performance and setup.
		// 3 - Per-iteration information
		// ...
		// 10 - Debugging information
	}
	~Chaos(){};

	static bool didInitializeMPI;
	static std::chrono::time_point<std::chrono::system_clock> tic, toc;
	static int logLevel;
	static inline void Init() {
		#ifdef CHAOS_MPI_FOUND
		int init; MPI_Initialized(&init);
		if (init == 0) {
			int requested;
			MPI_Init_thread(0,0, MPI_THREAD_SERIALIZED, &requested );
			if (requested < MPI_THREAD_SERIALIZED) std::cout << "WARNING: MPI could not provide MPI_THREAD_SERIALIZED support." << std::endl;
			Chaos::didInitializeMPI = true;
		}
		#endif
	}
	static inline void Finalize() {
		#ifdef CHAOS_MPI_FOUND
		int final; MPI_Finalized(&final);
		if (final == 0 && Chaos::didInitializeMPI)
			MPI_Finalize();
		#endif
	}

	static inline void GlobalBarrier() {
		ThreadBarrier();
		MPIBarrier();
		ThreadBarrier();
	}
	static inline void ThreadBarrier() {
		#pragma omp barrier
		{}
	}
	static inline void MPIBarrier() {
		#ifdef CHAOS_MPI_FOUND
		#pragma omp master
		MPI_Barrier(MPI_COMM_WORLD);
		#endif
	}
	static inline bool IsMPIUsed() {
		#ifdef CHAOS_MPI_FOUND
		return true;
		#else
		return false;
		#endif
	}
	static inline int GetRank() {
		int rank = 0;
		#ifdef CHAOS_MPI_FOUND
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		#endif
		return rank;
	}
	static inline void StartTimer() {
		tic = std::chrono::system_clock::now();
	}
	static inline void StopTimer() {
		toc = std::chrono::system_clock::now();
	}
	static inline unsigned int GetTimerMilliseconds() {
		return std::chrono::duration_cast<std::chrono::milliseconds>(toc-tic).count();
	}
	static inline std::string GetTimerMillisecondsString() {
		return std::to_string((long long)std::chrono::duration_cast<std::chrono::milliseconds>(toc-tic).count());
	}
	static inline std::string GetTimerMicrosecondsString() {
		return std::to_string((long long)std::chrono::duration_cast<std::chrono::microseconds>(toc-tic).count());
	}
	static inline void SetLogLevel(int level) {
		logLevel = level;
	}
	static inline void Log(std::string message, int minimumLogLevel, bool allProcesses = false) {
		if (GetRank() > 0 && !allProcesses) return;
		if (minimumLogLevel <= logLevel) {
			std::cout << message << std::endl;
		}
	}


private:

};


#endif