#include <iostream>
#include <vector>
#include <math.h>
#include "chaos.h"

struct Cell {
	int i,j,k; // ijk coordinates in physical space
	int proc;
	int idx; // local index, sequentially ordered on processors
	int globalidx; // global index, sequentially ordered on processors
};

int main (int argc, char * argv[]) {
	

	Chaos::Init();
	Chaos::SetLogLevel(4);

	//int nd = 64*2; 					// Number of grid cells in each direction
	if (argc == 1 ) std::cout << "Expected an argument N for the grid size (N^3)." << std::endl;
	int nd = atoi(argv[1]); 					// Number of grid cells in each direction
	double h = 1.0/nd;				// Size of grid cells in each direction

	// Each processor requires a mostly-square block of elements. Partitioning via global index is bad for parallel performance.
	// This program only works if the number of processors has a cube root.

	int nProcs = 1;
	#ifdef CHAOS_MPI_FOUND
		MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
	#endif
	int nPerDim = round(cbrt(nProcs));
	if (nProcs != nPerDim*nPerDim*nPerDim) {
		std::cout << "Number of processors must have a valid cube root (i.e. 1,8,27,64,...)" << std::endl;
		return -1;
	}

	// Create an array of cells for the local portion of the problem
	int iProc = 0;
	#ifdef CHAOS_MPI_FOUND
		MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
	#endif

	// Create all the cells for the problem
	std::vector < std::vector < std::vector < Cell* > > > cells;
	std::vector < Cell* > localCells;

	for (int i = 0; i < nd; i++) {
		std::vector<std::vector<Cell*> > row;
		cells.push_back( row );
		for (int j = 0; j < nd; j++) {
			std::vector<Cell*> col;
			cells[i].push_back( col );
			for (int k = 0; k < nd; k++) {
				Cell * cell = new Cell();
				cell->i = i;
				cell->j = j;
				cell->k = k;
				cells[i][j].push_back( cell );
			}
		}
	}

	int global_counter = 0;
	int global_start;
	for (int p = 0; p < nProcs; p++)
	{
		int local_counter = 0;
		if (local_counter == 0 && p == iProc) global_start = global_counter;
	 	int nd_local = nd/nPerDim;
	 	int i_pstart = nd_local * (p%nPerDim) ;
	 	int j_pstart = nd_local * ((p/nPerDim)%nPerDim) ;
	 	int k_pstart = nd_local * ((p/nPerDim/nPerDim)%nPerDim) ;
	 	int i_pend = i_pstart + nd_local;
	 	int j_pend = j_pstart + nd_local;
	 	int k_pend = k_pstart + nd_local;

	 	for (int i = i_pstart; i < i_pend; i++) {
			for (int j = j_pstart; j < j_pend; j++) {
				for (int k = k_pstart; k < k_pend; k++) {
					cells[i][j][k]->globalidx = global_counter;
					cells[i][j][k]->proc = p;
					global_counter++;
					
					if (p == iProc) {
						cells[i][j][k]->idx = local_counter;
						localCells.push_back( cells[i][j][k] );
						local_counter++;
					}
				}
			}
		}
	}



	Matrix<> A  ( localCells.size(), 			// Number of rows in the local portion of the matrix
	 			  localCells.size()*6, 			// Number of non-zeroes for memory allocation
	 			  global_start					// Global index where this processor begins
	 			);

	for(auto&& cell : localCells) {

		double diag = 0.0;
		std::vector<double> values;
		std::vector<int> globalColIdxs;
		int i = cell->i;
		int j = cell->j;
		int k = cell->k;

		diag += 6*h;

		/* In i-direction */
		if (i == 0) {
			//diag += 3*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i+1][j][k]->globalidx );
		} else if ( i == nd -1) {
			//diag += 3*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i-1][j][k]->globalidx );
		} else {
			//diag += 2*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i+1][j][k]->globalidx );
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i-1][j][k]->globalidx );
		}

		/* In j-direction */
		if (j == 0) {
			//diag += 3*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j+1][k]->globalidx );
		} else if ( j == nd -1) {
			//diag += 3*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j-1][k]->globalidx );
		} else {
			//diag += 2*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j+1][k]->globalidx );
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j-1][k]->globalidx );
		}

		/* In k-direction */
		if (k == 0) {
			//diag += 3*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j][k+1]->globalidx );
		} else if ( k == nd -1) {
			//diag += 3*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j][k-1]->globalidx );
		} else {
			//diag += 2*h;
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j][k+1]->globalidx );
			values.push_back ( -1*h );
			globalColIdxs.push_back( cells[i][j][k-1]->globalidx );
		}


		A.AddRow( diag, globalColIdxs, values );
	}

	A.Complete();
	
	// Debug: print the local matrix from processor 0
	//if (iProc == 0)
		// for (int i = 0; i < A.Size(); i++)
		// 	A.PrintRow(i);
	Chaos::GlobalBarrier();

	Vector<> x(&A, 0.0);
	Vector<> b(&A, 0.0);
	Vector<> r(&A, 0.0);

	for(auto&& cell : localCells) {
		x.at(cell->idx) = sin( ((double)cell->i+0.5)*h * ((double)cell->j+0.5)*h * ((double)cell->j+0.5)*h );
	}

	Chaos::GlobalBarrier();
	x.Scatter();
	A.Multiply(&x, &b);
	Chaos::GlobalBarrier();
	x.SetAll(0.0);


	// Matrix<> mat("../matrices/massTransport00005.mat");
	// Vector<> x("../matrices/massTransport00005.x", &mat);
	// Vector<> b("../matrices/massTransport00005.rhs", &mat);
	// Vector<> r(&mat, 0.0);

	Chaos::GlobalBarrier();
		
	Solver s;
	s.SetSolution( &x );
	s.SetRightHandSide( &b );
	s.SetResidualVector ( &r );
	s.SetRelativeConvergenceTolerance( 1e-8 );
	s.SetMaxIterations( 100000 );
	//s.SetMinimumGridSize(1000);
	s.SetMinimumGridSize(2);
	s.SetMinimumGridSizeRelative(0.0);
	s.SetAggregationFactor(8);
	s.SetMaxLevels(10000);
	s.SetMatrix( &A, false );

	s.SetPostSmoothIterations(30);
	s.SetPreSmoothIterations(30);
	s.SetCoarsestSmoothIterations(30);

    s.SetSolverType ( Solver::SolverType::MG_Chaotic );

	auto tic = std::chrono::system_clock::now();
	s.Solve();
	auto toc = std::chrono::system_clock::now();
	int timer = std::chrono::duration_cast<std::chrono::milliseconds>(toc-tic).count();

	if (Chaos::GetRank() == 0)
		std::cout << "Converged in " << s.GetIterations() << " iterations (" << timer << " ms)" << std::endl;

	// for(auto&& cell : localCells) {
	// 	std::cout << x.at(cell->idx) << " ==> " << sin( ((double)cell->i+0.5)*h * ((double)cell->j+0.5)*h * ((double)cell->j+0.5)*h ) << std::endl;
	// }


	for (int i = 0; i < nd; i++) {
		for (int j = 0; j < nd; j++) {
			for (int k = 0; k < nd; k++) {
				delete cells[i][j][k];
			}
		}
	}
	
	Chaos::Finalize();

}
