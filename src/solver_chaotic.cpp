#include "solver.h"
#include <omp.h>

bool Solver::ChaoticSolve() {

    int residualCheckInterval = 100;
    bool stop = false;

    iterations = 0;
	norm0 = ComputeResidual();

	#pragma omp parallel
	{
		int numComputeThreads = omp_get_num_threads() - 1;
		int commThread = omp_get_num_threads() - 1;
		int thread = omp_get_thread_num();

		// COMPUTE THREADS
		if (thread != commThread) { // All threads except the last thread
			int bottom = A->Size() * (double)thread/(double)numComputeThreads;
			int top = A->Size() *  (double)(thread+1)/(double)numComputeThreads;

			//printf("rank: %i, range %i to %i\n", thread, bottom, top);
			while(!stop) {
				for (int i = bottom; i < top; i++) {
					double rowSum = 0;
					for (int j = A->A.R[i]; j < A->A.R[i+1]; j++) {
						rowSum += A->A.V[j] * x->A[ A->A.C[j] ];
					}
					for (int j = A->B.R[i]; j < A->B.R[i+1]; j++ ) {
						rowSum += A->B.V[j] * x->B[ A->B.C[j] ];
					}
					x->A[i] = ( b->A[i] - rowSum ) / A->D[i];
				}
				// One thread updates the iteration count
				if (thread == 0)
					iterations++;
				// Flush the while-loop flag in case the communication thread has updated it in its local cache.
       			#pragma omp flush (stop)
			}
		}
		// COMMUNICATION THREAD
		else if (thread == commThread) {
			int communicationIterations = 0;
			while(!stop) {
				communicationIterations++;
				x->ScatterBegin();
				x->ScatterEnd();
				if (communicationIterations % residualCheckInterval == 0) {
					norm = ComputeResidual();
					//Chaos::Log("Communication iteration " + std::to_string((long long)communicationIterations) +": " + std::to_string((long double)norm) + "  (" + std::to_string((long long)iterations) + " computation iterations)" , 3);
                    if (IsConverged()) stop = true;
                    if (IsDiverged()) stop = true;
					#pragma omp flush(stop)
				}
			}
		}
	}

    return true;
}
