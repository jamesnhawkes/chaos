/* Classical Multigrid implementation with support for
 * 		V-Cycle, W-Cycle, F-Cycle and Sawtooth-Cycle
 * 		using async Gauss-Seidel smoothing */


#include "solver.h"

// This is not really Chaotic because the iterations are synchronous, but it is not deterministic either
// because of the multi-threading
void Solver::ClassicalMGSmooth( int level, int iterations, bool isParallel )
{
	int async3 = false;
	if (iterations < 0) {
		async3 = true;
		iterations = - iterations;
	}

	for (int m = 0; m < iterations; m++)
	{
		if (isParallel && (!async3 || m % 3 == 2) ){
			#pragma omp master
			{
				if (level == 0) {
					//Chaos::StartTimer();
					xs[level]->ScatterBegin();
					//Chaos::StopTimer();
					//Chaos::Log("StartScatter on level" + std::to_string((long long)level) + " took ("+Chaos::GetTimerMicrosecondsString() + "us)" , 1, true);
					//Chaos::StartTimer();
					xs[level]->ScatterEnd();
					//Chaos::StopTimer();
					//Chaos::Log("EndScatter on level" + std::to_string((long long)level) + " took ("+Chaos::GetTimerMicrosecondsString() + "us)" , 1, true);
				} else {
					//Chaos::StartTimer();
					xs[0]->ScatterBeginProxy( level, xs[level]->A, fcMaps );
					//Chaos::StopTimer();
					//Chaos::Log("StartScatter on level" + std::to_string((long long)level) + " took ("+Chaos::GetTimerMicrosecondsString() + "us)" , 1, true);
					//Chaos::StartTimer();
					xs[0]->ScatterEndProxy  ( level, xs[level]->B );
					//Chaos::StopTimer();
					//Chaos::Log("EndScatter on level" + std::to_string((long long)level) + " took ("+Chaos::GetTimerMicrosecondsString() + "us)" , 1, true);
				}
			}
			Chaos::ThreadBarrier();
		}

		//Chaos::StartTimer();
		#pragma omp for
		for (int i = 0; i < mats[level]->Size(); i++) {
			double rowSum = 0;
			for (int j = mats[level]->A.R[i]; j < mats[level]->A.R[i+1]; j++) {
				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
			}
			if (isParallel) {

				if (level == 0) {
					for (int j = mats[level]->B.R[i]; j < mats[level]->B.R[i+1]; j++ ) {
						rowSum += mats[level]->B.V[j] * xs[level]->B [ mats[level]->B.C[j] ];
					}
				} else {

					for (int fi = 0; fi < cfMaps[level][0][i].size(); fi++) {
						int f_idx = cfMaps[level][0][i][fi];
						for (int fj = mats[0]->B.R[  f_idx  ]; fj < mats[0]->B.R[f_idx+1]; fj++ ) {
							rowSum += mats[0]->B.V[fj] * xs[level]->B [ mats[0]->B.C[fj] ];
							//std::cout << "Scatter using  " << xs[level]->B [ mats[0]->B.C[fj] ] << std::endl;
						}
					}
				}
			}
			xs[level]->A[i] = ( bs[level]->A[i] - rowSum ) / mats[level]->D[i];
		}
		//Chaos::StopTimer();
		//Chaos::Log("Smooth on level" + std::to_string((long long)level) + " took ("+Chaos::GetTimerMicrosecondsString() + "us)" , 1, true);

	}

	if (isParallel){
		#pragma omp master
		{
			if (level == 0) {
				xs[level]->ScatterBegin();
				xs[level]->ScatterEnd();
			} else {
				xs[0]->ScatterBeginProxy( level, xs[level]->A, fcMaps );
				xs[0]->ScatterEndProxy  ( level, xs[level]->B );
			}
		}
		Chaos::ThreadBarrier();
	}


	//Chaos::GlobalBarrier();
}

// This is a deterministic Jacobi smoother
void Solver::ClassicalMGSmooth_Jacobi( int level, int iterations, bool isParallel )
{
	// Create the work vector if it doesn't exist
	#pragma omp master
	{
		work1[level]->Resize(mats[level]->Size());
	}
	Chaos::ThreadBarrier();

	for (int m = 0; m < iterations; m++)
	{

		#pragma omp for
		for (int i = 0; i < mats[level]->Size(); i++) {
			double rowSum = 0;
			for (int j = mats[level]->A.R[i]; j < mats[level]->A.R[i+1]; j++) {
				rowSum += mats[level]->A.V[j] * xs[level]->A[ mats[level]->A.C[j] ];
			}
			if (isParallel) {

				if (level == 0) {
					for (int j = mats[level]->B.R[i]; j < mats[level]->B.R[i+1]; j++ ) {
						rowSum += mats[level]->B.V[j] * xs[level]->B [ mats[level]->B.C[j] ];
					}
				} else {

					for (int fi = 0; fi < cfMaps[level][0][i].size(); fi++) {
						int f_idx = cfMaps[level][0][i][fi];
						for (int fj = mats[0]->B.R[  f_idx  ]; fj < mats[0]->B.R[f_idx+1]; fj++ ) {
							rowSum += mats[0]->B.V[fj] * xs[level]->B [ mats[0]->B.C[fj] ];
							//std::cout << "Scatter using  " << xs[level]->B [ mats[0]->B.C[fj] ] << std::endl;
						}
					}
				}
			}
			work1[level]->A[i] = ( bs[level]->A[i] - rowSum ) / mats[level]->D[i];
		}
		
		#pragma omp for
		for (int i = 0; i < mats[level]->Size(); i++) {
			xs[level]->A[i] = work1[level]->A[i];
		}

		if (isParallel){
			#pragma omp master
			{
				if (level == 0) {
					xs[level]->ScatterBegin();
					xs[level]->ScatterEnd();
				} else {
					xs[0]->ScatterBeginProxy( level, xs[level]->A, fcMaps );
					xs[0]->ScatterEndProxy  ( level, xs[level]->B );
				}
			}
			Chaos::ThreadBarrier();
		}
		
	}
}

void Solver::ClassicalMGRestrict(int f, int c)
{
	// Restrict residual into RHS (bs) of coarser level
	#pragma omp for
	for (int i = 0; i < xs[c]->B.size(); i++)
		xs[c]->B[i] = 0;
	#pragma omp for
	for (int i = 0; i < mats[c]->Size(); i++) {
		bs[c]->at(i) = 0;
		xs[c]->at(i) = 0; // Note resetting the coarse grid solution to zero, can't use previous values!
		// Sum the residual of the fine grid into the coarse grid RHS)
		for (int j = 0; j < cfMaps[c][f][i].size(); j++) {
			int idx = cfMaps[c][f][i][j];			
			// Compute residual (b-Ax) -- we are summing
			bs[c]->at(i) += ComputeRowResidual(idx, f);
		}
	}
}
void Solver::ClassicalMGSawtoothRestrict(int f, int c)
{
	// Same as above, but the residual on the finer level just equals b, because x=0 (no presmoothing and x initialized to zero)
	#pragma omp for
	for (int i = 0; i < xs[c]->B.size(); i++)
		xs[c]->B[i] = 0;
	#pragma omp for
	for (int i = 0; i < mats[c]->Size(); i++) {
		bs[c]->at(i) = 0;
		xs[c]->at(i) = 0; // Note resetting the coarse grid solution to zero, can't use previous values!
		// Sum the residual of the fine grid into the coarse grid RHS)
		for (int j = 0; j < cfMaps[c][f][i].size(); j++) {
			int idx = cfMaps[c][f][i][j];			
			// Compute residual (b-Ax) -- we are summing. In sawtooth, x=0 and therefore r=b
			bs[c]->at(i) += bs[f]->at(idx);
		}
	}
}

void Solver::ClassicalMGProlong(int c, int f)
{
	// Prolong	
	#pragma omp for
	for (int i = 0; i < mats[c]->Size(); i++) {
		// Add the solution (x) of (coarse) into (x) of (fine)
		for (int j = 0; j < cfMaps[c][f][i].size(); j++) {
			int idx = cfMaps[c][f][i][j];
			xs[f]->at(idx) += xs[c]->at(i); // TODO: Overrelaxation (1.8 ^ dimension)?
		}
	}
	
}

bool Solver::ClassicalMGSolve()
{
	if (numLevels == 1) {
		// Cant do multigrid
		for (int k = 0; k < maxIterations; k++) {
			ClassicalMGSmooth(0, preSmoothIterations + postSmoothIterations, true);
			iterations++;
			norm = ComputeResidual();
            Chaos::Log("Iteration " + std::to_string((long long)iterations) +": " + std::to_string((long double)norm), 3);
			if (IsConverged()) return true;
			if (IsDiverged()) return false;
		}
		return false;
	}

	switch( solverType )
	{
		// V-Cycle and W-cycle
		case(SolverType::MG_V):
		case(SolverType::MG_W):
			for (int k = 0; k < maxIterations; k++) {
				//Chaos::StartTimer();
				#pragma omp parallel
				{
                    ClassicalMGSmooth(0, preSmoothIterations, true);
					Chaos::ThreadBarrier();
                    ClassicalMGRestrict( 0, s );
                    Chaos::GlobalBarrier();
                    ClassicalMGRecursiveSolve(s);
					Chaos::ThreadBarrier();
                    ClassicalMGProlong( s, 0 );
					Chaos::ThreadBarrier();
                    ClassicalMGSmooth(0, postSmoothIterations, true);
					Chaos::ThreadBarrier();

					#pragma omp master
					{
						iterations++;
						norm = ComputeResidual();
                 		Chaos::Log("Iteration " + std::to_string((long long)iterations) +": " + std::to_string((long double)norm), 3);
					}
				}
				//Chaos::StopTimer();
				//Chaos::Log("Iteration using levels: " + std::to_string((long long)numLevels) + " took ("+Chaos::GetTimerMicrosecondsString() + "us)" , 1, true);
				if (IsConverged()) return true;
				if (IsDiverged()) return false;
			}
		break;

		case(SolverType::MG_F):
			for (int k = 0; k < maxIterations; k++) {
				#pragma omp parallel
				{
					std::vector<bool> HasVisited(numLevels, false);
					Chaos::ThreadBarrier();
                    ClassicalMGSmooth(0, preSmoothIterations, true);
					Chaos::ThreadBarrier();
                    ClassicalMGRestrict( 0, s );
                    Chaos::GlobalBarrier();
                    ClassicalMGRecursiveSolve(s, &HasVisited);
					Chaos::ThreadBarrier();
                    ClassicalMGProlong( s, 0 );
					Chaos::ThreadBarrier();
                    ClassicalMGSmooth(0, postSmoothIterations, true);
					
					#pragma omp master
					{
						iterations++;
						norm = ComputeResidual();
						Chaos::Log("Iteration " + std::to_string((long long)iterations) +": " + std::to_string((long double)norm), 3);
					}
				}
				if (IsConverged()) return true;
				if (IsDiverged()) return false;
			}
		break;

		case(SolverType::MG_Sawtooth):
			
			for (int k = 0; k < maxIterations; k++) {

				// Set all xs[] to zero
				for (int i = s; i < numLevels; i+=s)
				{
					xs[i]->SetAll(0.0);
				}

				#pragma omp parallel
				{
					// Restrict the finest level into the first coarse level as usual
					//Chaos::GlobalBarrier();
                    ClassicalMGRestrict(0, s);
					// Recursively restrict into all coarser levels, using the shortcut that all xs[]=0
					int level = s;
					while (level + s < numLevels - 1) {
						Chaos::ThreadBarrier();
                        ClassicalMGSawtoothRestrict(level, level+s);
						level += s;
					}
					level = coarsestSolverLevel;
					Chaos::GlobalBarrier();

					// Smooth on the coarsest level
                    ClassicalMGSmooth( level, coarsestSmoothIterations, true);

					// Recursively prolong and smooth on the way back up
					while (level - s <= 0) {
						Chaos::ThreadBarrier();
                        ClassicalMGProlong(level, level - s);
						level -= s;
						Chaos::ThreadBarrier();
                        ClassicalMGSmooth(level, postSmoothIterations, true);
					}
					// Prolong to the finest level
					Chaos::ThreadBarrier();
                    ClassicalMGProlong(level, 0);
					// Smooth with MPI communications
					Chaos::ThreadBarrier();
                    ClassicalMGSmooth(0, postSmoothIterations, true);
					
					#pragma omp master
					{
						iterations++;
						norm = ComputeResidual();
                 		Chaos::Log("Iteration " + std::to_string((long long)iterations) +": " + std::to_string((long double)norm), 3);
					}

					// if (mats[0]->GetParallelContext()->MPIrank == 0)
					// 		std::cout << norm/norm0 << std::endl;
				}
				if (IsConverged()) return true;
				if (IsDiverged()) return false;
			}
		break;

		
		default:
			std::cout << "The selected SolverType is not supported by the classical multigrid." << std::endl;
			return false;
			break;
		
	}
}

void Solver::ClassicalMGRecursiveSolve(int level, std::vector<bool> * HasVisited)
{
	if (level + s >= numLevels)
	{
        ClassicalMGSmooth( level, coarsestSmoothIterations, true);
	}
	else
	{
        ClassicalMGSmooth(level, preSmoothIterations, true);
		Chaos::ThreadBarrier();
        ClassicalMGRestrict( level, level + s );
        Chaos::GlobalBarrier();
        ClassicalMGRecursiveSolve(level + s, HasVisited);
		Chaos::ThreadBarrier();
        ClassicalMGProlong( level + s, level );
		Chaos::ThreadBarrier();
        ClassicalMGSmooth(level, postSmoothIterations, true);
		Chaos::ThreadBarrier();

		if (solverType == SolverType::MG_F){
			if (HasVisited->at(level) == false) {
				HasVisited->at(level) == true;
				Chaos::ThreadBarrier();
                ClassicalMGRestrict( level, level + s );
                Chaos::GlobalBarrier();
                ClassicalMGRecursiveSolve(level + s, HasVisited);
				Chaos::ThreadBarrier();
                ClassicalMGProlong( level + s, level );
				Chaos::ThreadBarrier();
                ClassicalMGSmooth(level, postSmoothIterations, true);
				Chaos::ThreadBarrier();
			}
		}

		if (solverType == SolverType::MG_W)
		{
            ClassicalMGRestrict( level, level + s );
            Chaos::GlobalBarrier();
            ClassicalMGRecursiveSolve(level + s);
			Chaos::ThreadBarrier();
            ClassicalMGProlong( level + s, level );
			Chaos::ThreadBarrier();
            ClassicalMGSmooth(level, postSmoothIterations, true);
		}
	}

}
