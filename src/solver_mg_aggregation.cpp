#include "solver.h"
// Routines and helper-classes to handle aggregation


#include <list>
#include <tuple>
#include <algorithm>
#include <vector>
#include <set>

#include "vector.h"

/* 
 * =================================================================================================================================
 * 
 * The basic principal here is to find sets of matrix rows which are strongly coupled. Elements are strongly coupled when
 * their corresponding off-diagonal elements are highly negative. Call these rows the donor rows.
 * 
 * Each coupled set is then "aggregated", and they become one row in the coarser matrix. A map from the coarse to fine grid (cfMap) can
 * be created, and vice versa (fcMap).
 * 
 * For each row in the coarse matrix, the diagonal is the sum of the donor diagonals minus plus the off-diagonal values that originally
 * coupled them.
 * 
 * For each off-diagonal in the donor rows, use the fcMap to determine a new column index for an off-diagonal element in the coarse
 * matrix. Keep the existing matrix value from the donor off-diagonal. If multiple off-diagonals reference the same coarse cell,
 * sum the matrix value.
 * 
 * =================================================================================================================================
 * 
 * A good technique for detecting strongly-coupled cells is pairwise aggregation. The proper mechanism looks at each cell and ranks its
 * neighbours by how coupled they are. It then sorts the cells so that most-coupled cells are aggregated first. During aggregation,
 * the algorithm checks that the coupling is reciprocal (i.e. X is strongly coupled to Y, but is Y strongly coupled to A?). This final
 * step is not necessary in a symmetric matrix. The process can be performed twice to create "double-pairwise" aggregation, aiming for
 * a factor-4 coarsening.
 * 
 * ==================================================================================================================================
 * 
 * Here I propose an alternative version which should be cheaper. Visit each row, find the "most-coupled" neighbour. If that neighbour 
 * is already taken, couple to the "next-most-coupled" neighbour, and so on. If no neighbours are found with a significant coupling,
 * then join with the neigbour that is part of the smallest aggregation.
 * 
 * DoAggregation2, Solver::Aggregate2 and MakeMatrixFromAgggregation implement this cheaper method. The other implementations (entry at
 * DoAggregation) are the pairwise scheme. 
 * 
 */
 

class Solver::Aggregate2 {
public:
	std::vector<int> fine_idxs;
	int coarse_idx;
};

 
Matrix<double> * Solver::DoAggregation2(Matrix<double> * fine) {

	std::vector<Aggregate2 *> aggregates;
	std::vector<Aggregate2 *> unique_aggregates;
	aggregates.resize( fine->Size() );
	
	std::vector<int> numAgged; std::vector<double> coupling;

	int maxAggSize = aggregationFactor;
	int coarse_idx_tracker = 0;


	for (int i = 0; i < fine->Size(); i++ ) {

		if (aggregates[i] != NULL && aggregates[i]->fine_idxs.size() >= maxAggSize) // We are already fully aggregated!
			continue;

		numAgged.clear(); coupling.clear();
		for (int j = fine->A.R[i]; j < fine->A.R[i+1]; j++) {

			if (  aggregates[ fine->A.C[j] ] == NULL ) {
				numAgged.push_back(1);
			} else {
				numAgged.push_back(  aggregates[ fine->A.C[j]]->fine_idxs.size() );
			}
			coupling.push_back( fine->A.V[j] );
		}

		// Connect to the cell with minimum numAgged and maximum coupling
		int minNumAgged = 1;
		int tracker = -1;
		while(true) {
			double maxCoupling = 0.0;// -0.05 * fine->D[i]; // Skip if weighting is high
			tracker = -1;
			for (int j = 0; j < coupling.size(); j++) {
				//if (numAgged[j] == minNumAgged) { // Group by minimum size of aggregates
				if (numAgged[j] < maxAggSize) { // Group by best coupling (until size of aggregate is too large)
					if (coupling[j] < maxCoupling) {
						maxCoupling = coupling[j];
						tracker = j;
					}
				}
			}
			if (tracker != -1) break;
			minNumAgged++;
			if (minNumAgged > maxAggSize -1) break;
		}

		if (tracker == -1) {
			// No valid coupling was found, make a new aggregate of just this element
			if (aggregates[i] == NULL) {
				aggregates[i] = new Aggregate2();
				aggregates[i]->fine_idxs.push_back(i);
				aggregates[i]->coarse_idx = coarse_idx_tracker;
				unique_aggregates.push_back(aggregates[i]);
				coarse_idx_tracker++;
			}
		} else {
			int idx =  fine->A.C[ fine->A.R[i] + tracker ];
			
			if (aggregates[idx] == NULL && aggregates[i] == NULL) {
				// Neither element belongs to an aggregate, make a new one
				aggregates[idx] = new Aggregate2();
				aggregates[idx]->fine_idxs.push_back(idx);
				aggregates[idx]->fine_idxs.push_back(i);
				aggregates[i] = aggregates[idx];
				aggregates[i]->coarse_idx = coarse_idx_tracker;
				unique_aggregates.push_back(aggregates[i]);
				coarse_idx_tracker++;
			} else if (aggregates[idx] == aggregates[i]){
				// Already coupled
			}
			else if (aggregates[idx] == NULL) {
				// This element belongs to an aggregate, put the coupled element in it
				aggregates[idx] = aggregates[i];
				aggregates[idx]->fine_idxs.push_back(idx);

			} else if (aggregates[i] == NULL) { /* this if statement should be superfluous */
				// This element does not belong to an aggregate, put us in the coupled element's aggregate
				aggregates[i] = aggregates[idx];
				aggregates[i]->fine_idxs.push_back(i);
			} else {
				// Both aggregates exist already, but they are different aggregates and cannot be coupled.
				//std::cout << "Aggregate could not be created." << std::endl;
			}
		}
	}

	// Now, we have a vector of (unique) aggregates which stores the index of fine elements which are coupled

	// Lets get some stats about the aggregation before we make the coarse matrix
	//int minCoupled = maxAggSize, maxCoupled = 0;
	//double averageCoupled = 0;
	//for (int i = 0; i < unique_aggregates.size(); i++) {
	//	int numCoupled = unique_aggregates[i]->fine_idxs.size();
	//	if (numCoupled < minCoupled) minCoupled = numCoupled;
	//	if (numCoupled > maxCoupled) maxCoupled = numCoupled;
	//	averageCoupled += (double)numCoupled;
	//}
	//averageCoupled /= unique_aggregates.size();

	//std::cout << "Aggregated: " << fine->Size() << " to " << unique_aggregates.size() << "(averageCoupled = " << averageCoupled << "  minCoupled = " << minCoupled << "  maxCoupled = " << maxCoupled << ")" << std::endl;

	return MakeMatrixFromAggregation(fine, aggregates, unique_aggregates);

}

Matrix<double> * Solver::MakeMatrixFromAggregation(Matrix<double> * fine, std::vector<Aggregate2*> & aggregates, std::vector<Aggregate2*> & unique_aggregates) {

	// Aggregates is a vector of all aggregates belonging to the fine matrix(with duplication)   [ A FINE TO COARSE MAP ]
	// Unique aggregates is a vector of all aggregates on the coarse matrix						 [ A COARSE TO FINE MAP ]
	
	// Get the size of the coarse matrix
	int coarse_size = unique_aggregates.size();
	// Create the coarse matrix
	Matrix<double> * cMat = new Matrix<double>(coarse_size,0, true);

	for (int i = 0; i < coarse_size; i++) {
		
		int coarse_idx = i;
		double sumDiag = 0;
		std::vector<double> offDiagonals;
		std::vector<int> colIdxs;

		// The diagonal is the diagonal of each fine_idx added together
		// PLUS the sum of all the off-diagonal elements which coupled them
		 
		// The remanining off-diagonals must be converted so they point to elements on the coarse grid
		
		for (int j = 0; j < unique_aggregates[coarse_idx]->fine_idxs.size(); j++) {
			int fine_idx = unique_aggregates[coarse_idx]->fine_idxs[j];

			// The diagonal of each fine row
			sumDiag += fine->D[fine_idx];

			// The offdiagonal of each fine row
			for (int k = fine->A.R[fine_idx]; k < fine->A.R[fine_idx+1]; k++ ) {

				// get the new coarse idx of the colIdx
				int coarse_col =  aggregates[fine->A.C[k]]->coarse_idx;
				
				// if the colIdx is THIS element, add it to the diagonal
				if (coarse_col == coarse_idx) {
					sumDiag += fine->A.V[k];
				} else {
				// if the colIdx already exists in the colIdxs vector, add the value to that index
					auto it = std::find (colIdxs.begin(), colIdxs.end(), coarse_col);
					if (it != colIdxs.end()) {
						// It already exists
						int pos = it - colIdxs.begin();
						offDiagonals[pos] += fine->A.V[k];
					} else {
					// else add it to the colIdx vector
						colIdxs.push_back(coarse_col);
						offDiagonals.push_back(fine->A.V[k]);
					}

				}
			}
		}
		
		cMat->AddLocalRow( sumDiag, colIdxs, offDiagonals );
	}

	cMat->Complete();

	// for (int r = 0; r < aggregates.size(); r++) {
	// 	std::cout << "original: " << r << ": ";
	// 	for (int i = 0; i < aggregates[r]->fine_idxs.size(); i++) {
	// 		std::cout << "\t" << aggregates[r]->fine_idxs[i];
	// 	}
	// 	std::cout << std::endl;
	// }


	// do cfMap and fcMap

	// CF MAP
	// Now, we need to build the ridiculous 4D vector which maps points to finer matrices
	// Lets add the maps for this coarse mesh
	cfMaps.push_back( std::vector<std::vector<std::vector<int>>>() );
	int c = cfMaps.size() -1; // Coarse matrix index
	// For all meshes which are finer, create a map (starting at finest = 0, coarsest = c-1)
	for (int f = 0; f < c; f++) {
		cfMaps[c].push_back( std::vector<std::vector<int>>() );
	}
	
	// For the matrix which is directly finer than this one, which we just used to build cMat
	int f = c -1; // Fine matrix index
	// TODO: parallelize this loop, reserve memory beforehand then direct access	                             
	for (int r = 0; r < cMat->Size(); r++ ) {
		cfMaps[c][f].push_back( std::vector<int>() );
		cfMaps[c][f][r] = unique_aggregates[r]->fine_idxs;

		// std::cout << "copied: " << r << ": ";
		// for (int i = 0; i < cfMaps[c][f][r].size(); i++) {
		// 	std::cout << "\t" << cfMaps[c][f][r][i];
		// }
		// std::cout << std::endl;
	}
	
	//std::cout << "Mapping " << c << " to " << f << std::endl;

	// Now, for every matrix that is finer than f, we need to create a map
	// Loop backwards (from coarsest-1 (f) to finest)
	for (int ff = f-1; ff >= 0; ff--)
	{
		// For each row in the matrix
		for (int r = 0; r < cMat->Size(); r++ ) {
			// Add a new vector
			cfMaps[c][ff].push_back(std::vector<int>());
			
			// Make a copy of the map we made last, from c to ff+1
			std::vector<int> temp = cfMaps[c][ff+1][r];
			// Now, expand that map on to ff by using the map of ff to ff+1
			for (int i = 0; i < temp.size(); i++) {
				//std::cout << temp[i] << std::endl;
				cfMaps[c][ff][r].insert ( cfMaps[c][ff][r].end(), cfMaps[ff+1][ff][temp[i]].begin(), cfMaps[ff+1][ff][temp[i]].end()  );
			}
		}
	}

	//std::cout << "Mapping " << f << " to " << c << std::endl;

	fcMaps.push_back(std::vector<std::vector<int>>() ); //fcMaps[c]

	// For each fine grid, add a map to this (coarse) level
	for (int ff = 0; ff < c; ff++)
		fcMaps[c].push_back(std::vector<int>());

	// Now, in reverse order, fill the map (i.e. starting with 5->4, 5->3,...,5->0)
	for (int ff = c-1; ff >= 0; ff--) { // fcMaps.size() = total number of grids

		// The first step, i.e. 5->4, where we use the aggregates array
		if (c-1 == ff) {
			for(int i = 0; i < aggregates.size(); i++) { // Size of fine mesh
				fcMaps[c][ff].push_back( aggregates[i]->coarse_idx );
			}
		// The remaining steps, i.e. 5->3 are recursive, using the map of 4->3
		} else {
			for (int i = 0; i < fcMaps[c-1][ff].size(); i++) {
				fcMaps[c][ff].push_back( aggregates[ fcMaps[c-1][ff][i] ]->coarse_idx );
			}
		}

	}

	// FOR VERIFICATION, PRINT THE CF AND FC MAP FROM 3->0 and 0->3
	// if (c == 3) {
	// 	std::cout << "Size of fcMap: " << fcMaps[c][0].size() << std::endl;
	// 	for (int i = 0; i < fcMaps[c][0].size(); i++)
	// 		std::cout << "\t" << i << " -> " << fcMaps[c][0][i] << std::endl;
	// 	for (int i = 0; i < cfMaps[c][0].size(); i++) {
	// 		std::cout << i << ": ";
	// 		for (int j = 0; j < cfMaps[c][0][i].size(); j++)
	// 			std::cout << "\t" << cfMaps[c][0][i][j];
	// 		std::cout << std::endl;
	// 	}
	// }

	return cMat;

}
 



/////////////////////////////////////
// 			  N O D E
/////////////////////////////////////

class Solver::Node {
public:
	Node(Matrix<double> * mat, int i, double beta) {
		double min = 0.0;
		
		idx = i;
		// Find strongest negative coupling
		// TODO: What if the (i.e. poisson) equation has negative diagonal and positive offdiagonals (i.e. inverted)?
		for (int j = mat->A.R[i]; j < mat->A.R[i+1]; j++) {
			double coefficient = mat->A.V[j];
			if (coefficient < min) min = coefficient;
		}
		for (int j = mat->A.R[i]; j < mat->A.R[i+1]; j++) {
			double coefficient = mat->A.V[j];
			if (coefficient < min*beta) {
				int idx = mat->A.C[j];
				coupled_to.push_back( std::pair<double,int>( coefficient, idx) );
			}
		}
		coupled_to.sort();
		// for(auto&& i : coupled_to) { std::cout << i.first << std::endl;}
	}
	~Node(){}
	void Build( std::vector<Node*> * nodes ) {
		// we are checking ALL nodes, this probably isn't necessary
		// in our matrices, we have structural symmetry	
		//for(auto&& node : *nodes) {
		for(auto&& pair : coupled_to) {
			Node * node = nodes->at(pair.second);
			for(auto&& couplednode : node->coupled_to) {
				if ( idx == couplednode.second ) {
					//std::cout << "Adding " << idx << std::endl;
					coupled_from.push_back(node);
				}
			}
		}
		//m = coupled_from.size();
	}
	int idx;
	std::list<std::pair<double,int> > coupled_to;
	std::list<Node*> coupled_from;
	//int m;
	void Forget(int idx, Node * node) {
		for(auto it = coupled_to.begin(); it != coupled_to.end(); it++) {
			auto pair = *it;
			if (it->second == idx) {
				coupled_to.erase( it );
				break;
			}
		}
		for(auto it = coupled_from.begin(); it != coupled_from.end(); it++) {
			auto node = *it;
			if (node->idx == idx) {
				coupled_from.erase( it );
				break;
			}
		}
	}
};


/////////////////////////////////////
// 	     A G G R E G A T E
/////////////////////////////////////

class Solver::Aggregate
{
public:
	Aggregate(int idx, Node * node1, Node * node2 = NULL) : idx(idx) {
		indices.push_back(node1->idx);
		if (node2 != NULL) indices.push_back(node2->idx);
	}
	Aggregate(int idx, Aggregate * agg1, Aggregate * agg2 = NULL) : idx(idx) {
		indices.insert( indices.end(), agg1->indices.begin(), agg1->indices.end());
		if (agg2 != NULL) indices.insert( indices.end(), agg2->indices.begin(), agg2->indices.end());
	}
	void Print() { 
		for( auto&& i: indices) std::cout << i << " ";
		std::cout << std::endl;
	}
	~Aggregate();
	int Size() { return indices.size(); }
	std::vector<int> indices;
	int idx;
};



Matrix<double> * Solver::DoAggregation(Matrix<double> * fine) {
	
	// A "node" is a single row of the fine matrix.
	std::vector<Node*> nodes;
	// Add each row
	//#pragma omp parallel for
	for (int i = 0; i < fine->Size(); i++) {
		nodes.push_back ( new Node( fine, i, strongCouplingCoefficient)  );
	}
	// Complete each node (collects all other nodes it is coupled to/from)
	#pragma omp parallel for
	for(int i = 0; i < nodes.size(); i++) {
		nodes[i]->Build( & nodes );
	}
	// Copy the vector of nodes* to a list for better sorting
	std::list<Node*> list_nodes;
	// TODO: Roll my own version of copy which uses openmp
	std::copy( nodes.begin(), nodes.end(), std::back_inserter( list_nodes ) );

	// Sort the nodes using a lambda function for comparison
	// We sort by the number of nodes to which a node is coupled_from (ascending)
	// Thus the least-coupled-to node is the first in the array
	// TODO: write a parallel version with openmp (need to write manually)
	list_nodes.sort([](const Node * const & s1, const Node * const & s2) {
		if (s1->coupled_from.size() <= s2->coupled_from.size())
			return true; else return false;
	});
	
	// An "aggregate" is a single row of the coarse matrix, consisting of multiple nodes.
	std::vector<Aggregate*> aggregates;
	
	// This is a map of nodes to aggregates, thus the i^th node can find out what aggregate it is in.
	std::vector<Aggregate*> i_to_aggregate;
	i_to_aggregate.resize(fine->Size());
	
	// Now, we do a while-loop over this list, starting from the front.
	// We keep looping until the list is empty
	// TODO: OpenMP this while-loop
	int while_counter = 0;
	while( !list_nodes.empty() ) {
		while_counter++;
		// Starting with least-coupled_from node: node1
		Node * node1 = list_nodes.front();
		// Get its strongest coupling -- if it has one!
		Node * node2;
		
		if ( node1->coupled_to.size() != 0 ) { // It has one
			// Strong coupled node: node2
			node2 = nodes [ node1->coupled_to.front().second ];
		} else {
			// It does not have one, create an aggregate with just node1
			// Store this aggregate in the mapping vector
			i_to_aggregate[node1->idx] = new Aggregate(aggregates.size(),node1);
			// Also store it in the vector of aggregates
			aggregates.push_back( i_to_aggregate[node1->idx] );
			// Take node1 out of the list
			list_nodes.pop_front();
			// Make sure all other nodes forget this node
			for(auto&& node : node1->coupled_from ) {
				node->Forget( node1->idx, node1 );
			}
			// Skip other checks
			continue;
		}

		// Now, does node1 exist in node2's coupled list?
		// i.e. are they coupled in both directions?
		bool found = false;
		for(auto&& pair : node2->coupled_to) {
			if (pair.second == node1->idx) {
				found = true;
			}
		}

		
		if (found) {
			// Yes, they are both coupled, create an aggregate from these two nodes
			// Store it in the mapping vector
			i_to_aggregate[node1->idx] = new Aggregate(aggregates.size(),node1,node2);
			i_to_aggregate[node2->idx] = i_to_aggregate[node1->idx];
			// Store it in the vector of aggregates
			aggregates.push_back( i_to_aggregate[node1->idx]);
			// Remove node1 (at the front) and node2 (somewhere in the list) from the list of nodes
			list_nodes.pop_front();
			list_nodes.erase( std::find(list_nodes.begin(), list_nodes.end(), node2) );

			// Tell the other nodes to forget node1 and node2
			for(auto&& node : node1->coupled_from ) {
				node->Forget( node1->idx, node1 );
			}
			for(auto&& node : node2->coupled_from ) {
				node->Forget( node2->idx, node2 );
			}
		} else {
			// No, node2 is not coupled to node1
			// node1 becomes an aggregate by itself, added to the map of aggregates
			i_to_aggregate[node1->idx] = new Aggregate(aggregates.size(), node1);
			// Added to the vector of aggregates
			aggregates.push_back( i_to_aggregate[node1->idx] );
			// Remove node1 from the list of nodes
			list_nodes.pop_front();
			// Tell other nodes to forget it
			for(auto&& node : node1->coupled_from ) {
				node->Forget( node1->idx, node1 );
			}
		}
		// We don't need to re-sort the list, thankfully. We could though, it does seem to improve coarsening.
		// TODO: test this some more
		/*if (while_counter % 100 == 0) {
		 	list_nodes.sort([](const Node * const & s1, const Node * const & s2) {
		 		if (s1->coupled_from.size() < s2->coupled_from.size())
		 		return true; else return false;
		 	});
		 }*/
	}
	
	
	// Get the size of the next coarsest matrix
	int coarse_size = aggregates.size();
	// Create the coarse matrix
	Matrix<double> * cMat = new Matrix<double>(coarse_size,0, true);
	//mats.push_back( cMat );
	
	// Here we need to get the connectivity of the aggregates
	// In principal, this is a nested loop [i,j]
	// which compares agg[i] and agg[j] to determine their coupling
	// However, the map which we stored earlier helps us to optimize this
	// because we know which agg[j]'s agg[i] is connected to.
	// Agg[i] has a list of fine rows {1,..,k}
	// Agg[j] has a list of fine rows {1,..,l}
	// These k/ls are what need to be compared
	
	// The first [i] loop, which will create each row of the coarse matrix from each aggregate
	// TODO: openmp
	for (int i = 0; i < coarse_size; i++) {
		
		double sumDiag = 0;
		std::vector<double> offDiagonal;
		std::vector<int> colIdx;
		
		// Aggregate[i]
		Aggregate * agg_i = aggregates[i];

		
		// Here, we create the diagonal entry by looking at all the fine rows contained in an aggregate
		// Take, for example, a case with two rows. Their two diagonals are added together.
		// From this sum, the off-diagonal elements which connect these two rows are deducted.
		// These off-diagonal elements are the same ones that coupled them in the first place.
		// Note that Agg[i] == Agg[j]
		for (int k = 0; k < agg_i->indices.size(); k++){
			// Sum diagonal elements
			sumDiag += fine->D[ agg_i->indices[k] ];//           / agg_i->indices.size();
			// Deduct off-diagonal elements, now we are looping over k and l
			for (int l = 0; l < agg_i->indices.size(); l++){
				sumDiag += fine->at( agg_i->indices[k], agg_i->indices[l] );//   / agg_i->indices.size(); // returns 0.0 on diagonal, so this just gets offdiagonals
				//std::cout << "+" << fine->at( k, l ) << "  ";
			}
		}
		double D = sumDiag;

		// Now, get all Agg[j]s that Agg[i] is coupled to, by looping over k,
		// 	and using the map to get the Agg[j]s
		std::set<Aggregate*> agg_js;
		for (int k = 0; k < agg_i->indices.size(); k++){			
			// Get ColIDX of row agg_i->indices[k]
			int rowPtrBot = fine->A.R[ agg_i->indices[k]];
			int rowPtrTop = fine->A.R[ agg_i->indices[k]+1];
			for (int m = rowPtrBot; m < rowPtrTop; m++ ) {
				agg_js.insert( i_to_aggregate[ fine->A.C[m] ]  );
			}
		}
				
		
		// Now we have a unique set of Agg[j]s
		// For each one we must construct an off-diagonal entry in the coarse matrix.
		
		//std::cout << "agg_j for i="<<i << ": " << agg_js.size()-1 << std::endl;
		// Loop over agg[j]s
		//for (auto&& agg_j : agg_js) {
		for (auto&& agg_j : agg_js) {
			// Off-diagonal
			double offdiag_sum = 0.0;
			
			// ... Ignore ourself (diagonal)
			if (agg_i != agg_j) {
				// ... double loop over k and l
				for (int k = 0; k < agg_i->indices.size(); k++){
					for (int l = 0; l < agg_j->indices.size(); l++){
						
						
						// ...get the off-diagonal at [k,l] in the fine mesh
						// (will return 0.0 if it does not exist in the sparse structure)
						double value = fine->at( agg_i->indices[k], agg_j->indices[l] );//  / agg_i->indices.size();
						//std::cout << agg_i->indices[k] << " " << agg_i->indices[l] << std::endl;
						// Sum any non-zero values
						if (value != 0.0)
							offdiag_sum += value;
						/*if (value != 0.0 && k!=l)
							offdiag_sum += value;
						else if (value != 0.0)
							offdiag_sum -= value;*/
					}
				}
			}
			// Finally, if the sum of these elements is non-zero
			// ... add this to the row in the coarse matrix
			if (offdiag_sum != 0.0) {
				offDiagonal.push_back(offdiag_sum);
				colIdx.push_back(agg_j->idx);
			}
		
		
		}

		// if (coarse_size < 6) {
		// 	std::cout << "Size is " << coarse_size << std::endl;
		// 	for (int k = 0; k < agg_i->indices.size(); k++){
		// 		std::cout << "D+=  " << fine->D[ agg_i->indices[k] ] << std::endl;
		// 		for (int l = 0; l < agg_i->indices.size(); l++){
		// 			std::cout << "D+=" << fine->at( agg_i->indices[k], agg_i->indices[l] ) << std::endl;
		// 		}
		// 	}
		// 	std::cout << "Adding row diag " << D << std::endl;
		// }

		// ... And add the row to the matrix
		cMat->AddLocalRow( D, colIdx, offDiagonal );
	}

	cMat->Complete();


	// Now, we need to build the ridiculous 4D vector which maps points to finer matrices
	// Lets add the maps for this coarse mesh
	cfMaps.push_back( std::vector<std::vector<std::vector<int>>>() );

	int c = cfMaps.size() -1; // Coarse matrix index
	
	// For all meshes which are finer, create a map (starting at finest = 0, coarsest = c-1)
	for (int f = 0; f < c; f++) {
		cfMaps[c].push_back( std::vector<std::vector<int>>() );
	}
	
	
	// For the matrix which is directly finer than this one, which we just used to build cMat
	int f = cfMaps[c].size() -1; // Fine matrix index
	// TODO: parallelize this loop, reserve memory beforehand then direct access	                             
	for (int r = 0; r < cMat->Size(); r++ ) {
		cfMaps[c][f].push_back( std::vector<int>() );
		cfMaps[c][f][r] = aggregates[r]->indices;
	}
	
	//std::cout << "c: " << c << std::endl;
	//std::cout << "f: " << f << std::endl;
	
	// Now, for every matrix that is finer than f, we need to create a map
	// so that we can go directly from c=4, f=0 (for example)	
	// Loop backwards (from coarsest-1 (f) to finest)
	for (int ff = f-1; ff >= 0; ff--)
	{
		// Add the map from c to ff
		//cfMaps[c].push_back (std::vector<std::vector<int>>());
		
		// For each row in the matrix
		for (int r = 0; r < cMat->Size(); r++ ) {
			// Add a new vector
			cfMaps[c][ff].push_back(std::vector<int>());
			
			// Make a copy of the map we made last, from c to ff+1
			std::vector<int> temp = cfMaps[c][ff+1][r];
			// Now, expand that map on to ff by using the map of ff to ff+1
			for (int i = 0; i < temp.size(); i++) {
				//std::cout << temp[i] << std::endl;
				cfMaps[c][ff][r].insert ( cfMaps[c][ff][r].end(), cfMaps[ff+1][ff][temp[i]].begin(), cfMaps[ff+1][ff][temp[i]].end()  );
			}
		}
	}

	// if (c ==2) {
	// 	//std::cout << "Size of fcMap: " << fcMaps[2][0].size() << std::endl; // should be size 1000
	// 	//for (int i = 0; i < fcMaps[2][0].size(); i++)
	// 	//	std::cout << "\t" << i << " -> " << fcMaps[2][0][i] << std::endl;
	// 	for (int i = 0; i < cfMaps[2][0].size(); i++) {
	// 		std::cout << i << ": ";
	// 		for (int j = 0; j < cfMaps[2][0][i].size(); j++)
	// 			std::cout << "\t" << cfMaps[2][0][i][j];
	// 		std::cout << std::endl;
	// 	}
	// }

	return cMat;

	
}










