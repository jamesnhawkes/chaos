%begin %{
  #include <cmath>
%}

 %module chaos
 %{
  #include "vector.h"
  #include "matrix.h"
  #include "chaos.h"
 %}
 
 /* Parse the header file to generate wrappers */
  %include "vector.h"
  %include "matrix.h"
  %include "chaos.h"
  %include "matrix.cpp"
  %include "vector.cpp"
  %include "std_string.i"

  %template(VectorD) Vector<double>;
  %template(MatrixD) Matrix<double>;
