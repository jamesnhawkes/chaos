#ifndef __CHAOS_VECTOR_H__
#define __CHAOS_VECTOR_H__

#include "chaos_private.h"

#include "parallelcontext.h"

#include <vector>
#include <cmath>
#include <iostream>
#include <string>

template<typename T>
class Matrix;

//! A basic vector class.
/*!
 * This is the basic Vector class for Chaos. It can be used to create serial vectors (much like std::vector), which can be resized freely,
 * but it's main purpose is to provide a container for parallel vectors. It is responsible for some mathematical functions, such as
 * computing norms or sums of itself. It also contains all the necessary communication functions for parallel vectors.
 * 
 * The local (shared-memory) portion of the vector is stored in a std::vector (A). A buffer of "halo" data is stored in (B). The user must provide
 * the local values for A. For parallel Vectors, the user must provide a Matrix or a ParallelContext, which tells the Vector how to assemble B, and
 * how to perform communication.
 * 
 * In the Matrix class, the same decomposition is found (A for the local portion, B for off-process elements).
 * Operations involving multiple Vectors or Matrices require that the ParallelContext of each object matches -- in other words, their
 * A's and B's must match.
 * 
 * Currently all Vector initialization is done by value (i.e. a Vector is always a copy of initialization data). This could be changed in
 * the future.
 * 
 * @tparam T The type of object stored in the Vector. By default, this is double.
 */
template <typename T = double>
class Vector
{
public:	
	/******************************************************************
	 * 				   C O N S T R U C T O R S
	 ******************************************************************/
	
	/*! Construct an uninitialized serial vector.
	 * @param size The initial (variable) size of the Vector.
	 */
	Vector ( int size = 0);
	
	/*! Construct a serial vector, initialized to an array of values.
	 * @param size The initial (variable) size of the Vector.
	 * @param values An array of values, with matching size.
	 */
	Vector ( int size, T * values );
	
	/*! Construct a serial vector, initialized to a value.
	 * @param size The initial (variable) size of the Vector.
	 * @param value Every value in the Vector will be initialized to this value.
	 */
	Vector ( int size, T value );
	
	/*! Construct a parallel vector, with uninitialized values.
	 *  @note 1 local communication pattern (for initialization of the buffer, B).
	 *  @param parallelContext The ParallelContext which defines the parallel distribution of the Vector.
	 */
	Vector ( ParallelContext * parallelContext );
	
	/*! Construct a parallel vector, initialized to an array of values.
	 *  @note 1 local communication pattern (for initialization of the buffer, B).
	 *  @param parallelContext The ParallelContext which defines the parallel distribution of the Vector.
	 *  @param values An array to copy into the local portion of the Vector (A). The size must match
	 *  the size of the ParallelContext.
	 */
	Vector ( ParallelContext * parallelContext, T * values );
	
	/*! Construct a parallel vector, with uninitialized values.
	 *  @note 1 local communication pattern (for initialization of the buffer, B).
	 *  @param associated_mat The Vector will adopt the ParallelContext of associated_mat, so their parallel distributions match.
	 */
	Vector ( Matrix<T> * associated_mat);
	
	/*! Construct a parallel vector, initialized to an array of values.
	 *  @note 1 local communication pattern (for initialization of the buffer, B).
	 *  @param associated_mat The Vector will adopt the ParallelContext of associated_mat, so their parallel distributions match.
	 *  @param values An array to copy into the local portion of the Vector (A). The size must match
	 *  the size of the ParallelContext.
	 */
	Vector ( Matrix<T> * associated_mat, T * values);
	
	/*! Construct a parallel vector, initialized to a single value.
	 *  @note It is assumed that value is the same on every process that calls this function, so the buffer (B) is also initialized
	 * to value. No communications are required. To circumvent this limitation, call Scatter() on the Vector to update the buffer
	 * manually.
	 *  @param parallelContext The ParallelContext which defines the parallel distribution of the Vector.
	 *  @param value A value to initialize the local portion of the Vector (A).
	 */
	Vector ( ParallelContext * parallelContext,  T value );
	
	/*! Construct a parallel vector, initialized to a single value.
	 *  @note It is assumed that value is the same on every process that calls this function, so the buffer (B) is also initialized
	 * to value. No communications are required. To circumvent this limitation, call Scatter() on the Vector to update the buffer
	 * manually.
	 *  @param associated_mat The Vector will adopt the ParallelContext of associated_mat, so their parallel distributions match.
	 *  @param value A value to initialize the local portion of the Vector (A).
	 */
	Vector ( Matrix<T> * associated_mat, T value);
	
	/*! Construct a serial vector from a file
	 * @note The file must be of the following format:\n
	 * 3\n
	 * 1: 1.0\n
	 * 2: 0.5\n
	 * 3: 3.0\n
	 * 
	 * @param fileName Relative or absolute path of the file
	 */
	Vector ( std::string fileName );
	
	/*! Construct a parallel vector from a file
	 * @see Vector ( std::string fileName )
	 * @param fileName Relative or absolute path of the file
	 * @param parallelContext The ParallelContext which defines the parallel distribution of the Vector.
	 */
	Vector ( std::string fileName, 	ParallelContext * parallelContext);
	
	/*! Construct a parallel vector from a file
	 * @see Vector ( std::string fileName )
	 * @param fileName Relative or absolute path of the file
	 * @param associated_mat The Vector will adopt the ParallelContext of associated_mat, so their parallel distributions match.
	 */
	Vector ( std::string fileName, 	Matrix<T> * associated_mat);
	
	/*! Destructor */
	~Vector() {}
	
	/******************************************************************
	 * 				 M E M B E R   F U N C T I O N S
	 ******************************************************************/
	
	/*! Initializes the vector by copying an array of values and performs a vector scatter to update the parallel buffer */
	void SetFromArray (T * values);

	/*! Resizes the local portion of the vector (A). */
	void Resize(int size);
	
	/*! Reserves memory space for the local portion of the vector (A), but does not change the size. */
	void Reserve(int size);
	
	/*! Adds an element to the end of the local vector (A), resizing if necessary */
	void Add(T value);
	
	/*! Optimize memory usage of A */
	void Optimize();
	
	/*! Performs a scatter of the parallel vector, updating the buffer (B) using variables in (A) of neighbouring processes.
	 * @note Requires 1 neighbour-to-neighbour communication.
	 */
	void Scatter();
	
	/*! Performs a non-blocking scatter of the parallel vector using the local values of neighbouring processes.
	 * This function packages the necessary elements from (A) and sends them to other processes. It sends them using non-blocking messages.
	 * This function must be accompanied by a matching ScatterEnd() which receives messages from other processes and packs them into (B).
	 * Access to (B) between ScatterBegin() and ScatterEnd() is non-deterministic and may cause data-races.
	 * @note When matched with ScatterEnd(), performs one non-blocking, asynchronous neighbour-to-neighbour communication.
	 * @see ScatterEnd()
	 * @see Scatter()
	 */
	void ScatterBegin();
	
	/*! Finishes the non-blocking scatter invoked by ScatterBegin().
	 * This function blocks until all messages have been received. It is safe to access the buffer (B) after calling this function.
	 * @note When matched with ScatterBegin(), performs one non-blocking, asynchronous neighbour-to-neighbour communication.
	 * @see ScatterBegin()
	 * @see Scatter()
	 */
	void ScatterEnd();

	/*! Performs a non-blocking scatter of the parallel vector using the local values of neighbouring processes.
	 * This function packages the necessary elements from (A) and sends them to other processes. It sends them using non-blocking messages.
	 * This function must be accompanied by a matching ScatterEnd() which receives messages from other processes and packs them into (B).
	 * Access to (B) between ScatterBegin() and ScatterEnd() is non-deterministic and may cause data-races.
	 * 
	 * The proxy version of this call takes a fine-to-coarse mapping to fill the vector buffer before sending
	 * 
	 * @note When matched with ScatterEnd(), performs one non-blocking, asynchronous neighbour-to-neighbour communication.
	 * @see ScatterEnd()
	 * @see Scatter()
	 */
	void ScatterBeginProxy(int coarse, std::vector<T> & source, std::vector<std::vector<std::vector<int>>> & fcMaps);
	void ScatterEndProxy(int coarse, std::vector<T> & destination);

	/*! Computes the local l^2-norm of the vector. */
	T NormLocal();
	
	/*! Computes the global l^2-norm of the vector.
	 * @note Requires 1 global communication.
	 */
	T Norm();

	/*! Begins asynchronous communication for a non-blocking l^2-norm computation of the vector.
	 */
	void NormBegin();
	/*! Ends asynchronous communication for a non-blocking l^2-norm computation of the vector.
	 */
	T NormEnd();
	
	/*! Computes the local sum of the vector. */
	T SumLocal();
	
	/*! Computes the global sum of the vector.
	 * @note Requires 1 global communication.
	 */
	T Sum();
	
	/*! Gets the size of the local portion of the Vector (A) */
	int Size() { return A.size(); }
	
	/*! Returns a copy of the ith element in the local portion (A) of the vector.
	 * @param i index of value in local vector.
	 */
	inline T & at(int i) { return A[i]; }
	
	/*! Returns a copy of the ith element in the local portion (A) of the vector.
	 * @param i index of value in local vector.
	 */
	inline T & operator[](int i) { return at(i); }
	
	/*! Returns a copy of the ith element in the local portion (A) of the vector.
	 * @param i index of value in local vector.
	 */
	inline T & atA(int i) { return A[i]; }
	
	/*! Returns a copy of the ith element in the buffer portion (B) of the vector.
	 * @param i index of value in local vector.
	 */
	inline T & atB(int i) { return B[i]; }
	
	/*! Sets all values in the Vector to a value.
	 * @param value
	 */
	void SetAll(T value);
	
	/*! Pretty-prints summary information about the Vector to stdout or a file
	 * @param fileName If given, redirects the output to a file.*/
	//void PrintSummary(std::string fileName = "");
	/*! Pretty-prints the local Vector values to stdout or a file
	 * @param fileName If given, redirects the output to a file.*/
	//void PrintDetail(std::string fileName = "");
	/*! Pretty-prints the Vector values to stdout or a file
	 * @param fileName If given, redirects the output to a file.*/
	//void Print(std::string fileName = "");

	/*! The local portion of this Vector */
	std::vector<T> A;
	/*! The buffer portion of this Vector */
	std::vector<T> B;
	/*! The parallel context of this Vector */
	ParallelContext * parallelContext;

	inline ParallelContext * GetParallelContext() { return parallelContext; }

	/*! A buffer to pack messages before they are sent */
	std::vector<T> sendbuf;

	void SetParallelContext(ParallelContext * parallelContext);	
	
protected:	
	bool isParallel;

	T myVal;
	#ifdef CHAOS_MPI_FOUND
		MPI_Request request;
		T allVal;
		std::vector<MPI_Request> irequests;
	#endif

private:
};

#endif