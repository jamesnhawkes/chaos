#ifndef __SCATTER_H
#define __SCATTER_H

#include <iostream>
#include <list>
#include <vector>
#include <math.h>
#include <iterator>
#include <algorithm>

#include "chaos_private.h"

/*! Handles data communication and other functions related to distributed memory systems.

    For example, this class contains the necessary information for scattering vectors across MPI processes.
	It also contains local-to-global mapping (and vice-versa) should that be necessary.
*/
class ParallelContext
{
public:
	ParallelContext(int globalsize, bool forceSerial = false) {
		this->forceSerial = forceSerial;
		GetRankAndSize();
		rangeBottom = round(((double)globalsize) * (double)MPIrank / (double)MPIsize);
		rangeTop = round(((double)globalsize) * ((double)MPIrank + 1.0) / (double)MPIsize) - 1;
		size = rangeTop - rangeBottom + 1;
		CommunicateNodeBounds();
	}

	ParallelContext(int localSize, int globalBegin){
		this->forceSerial = false;
		GetRankAndSize();
		rangeBottom = globalBegin;
		rangeTop = globalBegin + localSize -1;
		size = localSize;
		CommunicateNodeBounds();
	}
	
	void GetRankAndSize() {
		#ifdef CHAOS_MPI_FOUND
			if (!forceSerial) {
				MPI_Comm_rank(MPI_COMM_WORLD, &MPIrank);
				MPI_Comm_size(MPI_COMM_WORLD, &MPIsize);
			} else {
				MPIrank = 0;
				MPIsize = 1;
			}
		#else
			MPIrank = 0;
			MPIsize = 1;
		#endif
	}
	
	void CommunicateNodeBounds() {
		// Find the bounds of all the other processes
		
		nodeBounds = new int[MPIsize * 2];
		int* sendbuf = new int[2]{ rangeBottom, rangeTop };
		int* recsize = new int[MPIsize];
		int* sendsize = new int[MPIsize];
		int* senddispl = new int[MPIsize];
		int* rdispls = new int[MPIsize];
		for (int i = 0; i < MPIsize; i++) {
			rdispls[i] = i * 2;
			recsize[i] = 2;
			sendsize[i] = 2;
			senddispl[i] = 0;
		}
		#ifdef CHAOS_MPI_FOUND
			if (!forceSerial)
				MPI_Alltoallv(sendbuf, sendsize, senddispl, MPI_INT, nodeBounds, recsize, rdispls, MPI_INT, MPI_COMM_WORLD);
			else {
				nodeBounds[0] = sendbuf[0];
				nodeBounds[1] = sendbuf[1];
			}
		#else
			nodeBounds[0] = sendbuf[0];
			nodeBounds[1] = sendbuf[1];
		#endif

		delete[](sendbuf); delete[](recsize); delete[](sendsize); delete[](senddispl); delete[](rdispls);
		
		
		// DEBUG: Prints the bounds of each processor (from the master proc)
		// if (MPIrank == 0)
		// for (int i = 0; i < MPIsize; i++)
		// 	std::cout << "P" << i << ": \t" << nodeBounds[i * 2] << " -> " << nodeBounds[i * 2 + 1] << std::endl;
		return;
	}
	
	~ParallelContext(){
		delete[](nodeBounds);
		delete[](rec_buf_proc_offset);
		delete[](send_buf_cols);
		delete[](send_buf_proc_offset);
	}
	bool forceSerial;
	int rec_buf_size, send_buf_size;
	// Rank of MPI process and size of communicator
	int MPIrank, MPIsize;
	// nodeBounds = 2*mpiSize array with [i] = proc_min, [i+1] = proc_max
	int * nodeBounds;
	int rangeBottom, rangeTop, size;
	// rec_buf_proc_offset = a CRS style list of processor offsets of size mpiSize+1
	int * rec_buf_proc_offset;
	// send_buf_cols = the local columns which must be packed into the send buffer
	// send_buf_proc_offset = a CRS style list of processor offsets of size mpiSize+1
	int * send_buf_cols, *send_buf_proc_offset;
	
	template<typename T>
	void ComputeScatter(T * mat)
	{
		// Get a unique list of (global) column indexes, sorted in ascending order
		std::list<int> unique_sorted_list;
		for (int i = 0; i < mat->B.NZ(); i++)
			unique_sorted_list.push_back(mat->B.C[i]);

		unique_sorted_list.sort();
		unique_sorted_list.unique();

		// And convert it back to a vector (don't try to use the list now)
		std::vector<int> offProcRecvCols{ std::make_move_iterator(unique_sorted_list.begin()),
										  std::make_move_iterator(unique_sorted_list.end()) };

		//DEBUG: Prints the sorted list of global column indices
		/*for(auto&& i : offProcRecvCols) {
		 	std::cout << " " << i;
		}
		std::cout << std::endl;*/


		// std::cout << "Receive buffer of size N needed: " << offProcRecvCols.size() << std::endl;
		// std::cout << rank << ": Buffer[0] with colIdx " << offProcRecvCols[127] << " is on proc " <<
		// GetProc(offProcRecvCols[127], start_end_array) << std::endl;

		// procRecvBufferOffset stores the index at which each processors buffer begins, the size of each buffer can be
		// determined as in CRS storage [i+1] - [i]
		// the local process is included, even though its size will always be zero
		// note that offProcRecvCols is sorted in ascending order, so this works
		rec_buf_proc_offset = new int[MPIsize + 1];
		rec_buf_proc_offset[0] = 0;
		for (int i = 1; i < MPIsize + 1; i++)
			rec_buf_proc_offset[i] = -1;
		for (unsigned int i = 0; i < offProcRecvCols.size(); i++) {
			int proc = GetProc(offProcRecvCols[i]);
			// std::cout << proc << std::endl;
			// We are still on (proc). So the next processor (proc+1) hasn't found its range yet
			rec_buf_proc_offset[proc + 1] = i + 1;
		}
		for (int i = 1; i < MPIsize + 1; i++)
			if (rec_buf_proc_offset[i] == -1)
				rec_buf_proc_offset[i] = rec_buf_proc_offset[i - 1];

		// for (int i = 0; i < MPIsize; i++) {
		// 	int buffersize = procRecvBufferOffset[i+1] - procRecvBufferOffset[i];
		// 	std::cout << rank << ": Proc"<< i << " recv buffer of size " << buffersize << " starts at "  <<
		// procRecvBufferOffset[i] << "{ ";
		// 	for (int j = procRecvBufferOffset[i]; j < procRecvBufferOffset[i+1]; j++) {
		// 		std::cout << offProcRecvCols[j] - procStart << " ";
		// 	}
		// 	std::cout << "}" << std::endl;
		// }

		for (int i = 0; i < MPIsize; i++) {
		int buffersize = rec_buf_proc_offset[i + 1] - rec_buf_proc_offset[i];
			int bufferstart = rec_buf_proc_offset[i];
			// if (buffersize > 0) {
			#ifdef CHAOS_MPI_FOUND
				// Send a message which contains the global column indices required by this process
				// to rank [i] with tag [i]
				if (!forceSerial) {
					MPI_Request r;
					MPI_Isend(&offProcRecvCols[bufferstart], buffersize, MPI_INT, i, i, MPI_COMM_WORLD, &r);
					MPI_Request_free(&r);
				}
			#endif
			//}
		}

		#ifdef CHAOS_MPI_FOUND
			MPI_Message* messages = new MPI_Message[MPIsize + 1];
		#endif

		send_buf_proc_offset = new int[MPIsize + 1];
		send_buf_proc_offset[0] = 0;
		for (int i = 0; i < MPIsize; i++) {
			#ifdef CHAOS_MPI_FOUND
				MPI_Status status;
				// from rank [i] with tag [rank]
				if (!forceSerial) {
					MPI_Mprobe(i, MPIrank, MPI_COMM_WORLD, &messages[i], &status);
					int buffersize;
					MPI_Get_count(&status, MPI_INT, &buffersize);
					send_buf_proc_offset[i + 1] = send_buf_proc_offset[i] + buffersize;
				}
				else 
					send_buf_proc_offset[i + 1] = send_buf_proc_offset[i] + 0;
			#else
				send_buf_proc_offset[i + 1] = send_buf_proc_offset[i] + 0;
			#endif
		}

		// DEBUG
		// std::cout << procSendBufferOffset[MPIsize] << std::endl;
		// return;

		send_buf_cols = new int[send_buf_proc_offset[MPIsize]];
		for (int i = 0; i < MPIsize; i++) {
			int buffersize = send_buf_proc_offset[i + 1] - send_buf_proc_offset[i];
			int starts_at = send_buf_proc_offset[i];
			#ifdef CHAOS_MPI_FOUND
				if (!forceSerial)
					MPI_Mrecv(&send_buf_cols[starts_at], buffersize, MPI_INT, &messages[i], MPI_STATUS_IGNORE);
			#endif
		}

		// Convert to local row indices
		for (int i = 0; i < send_buf_proc_offset[MPIsize]; i++) {
			send_buf_cols[i] -= rangeBottom;
		}

		// Finally, take the original column map (B.C) and remap it to the RecvBuffer
		for (int i = 0; i < mat->B.NZ(); i++) {
			// This is like find, but faster because it takes advantage of offProcRecvCols being sorted
			auto lower = std::lower_bound(offProcRecvCols.begin(), offProcRecvCols.end(), mat->B.C[i]);
			// std::cout << remote.C->at(i) << " maps to buffer index " << std::distance(offProcRecvCols.begin(), lower) <<
			// std::endl;
			int idx = std::distance(offProcRecvCols.begin(), lower);
			// if(rank ==0) std::cout << remote.C->at(i) << " maps to -> " << idx << std::endl;
			mat->B.C[i] = idx;
		}

		/*for (int i = 0; i < MPIsize; i++) {
		 	int buffersize = send_buf_proc_offset[i+1] - send_buf_proc_offset[i];
		 	std::cout << MPIrank << ": Proc"<< i << " send buffer of size " << buffersize << " starts at "  <<
			send_buf_proc_offset[i] << "{ ";
			for (int j =  send_buf_proc_offset[i]; j < send_buf_proc_offset[i+1]; j++) {
		 		std::cout << send_buf_cols[j] << " ";
			}
			std::cout << "}" << std::endl;
		}*/

		#ifdef CHAOS_MPI_FOUND
			delete[] messages;
		#endif


		rec_buf_size = rec_buf_proc_offset[MPIsize];
		send_buf_size = send_buf_proc_offset[MPIsize];
	}

	int GetProc(int col)
	{
		int proc = 0;
		/* nodeBounds is the inclusive range, so this while loop continues until the column is found in a processors range */
		/* it should never return a processor rank higher than MPISize, but valgrind complains here */
		while ( /*proc < maxprocs?*/ col < nodeBounds[proc * 2] || col > nodeBounds[proc * 2 + 1]) {
			proc++;
		}
		return proc;
	}

	
};

#endif