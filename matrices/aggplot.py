import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

with open("agg1.dat", "r") as f:
	for line in f:
		l = line.split()
		if len(l) > 1:
			i = int(l[0]); j = int(l[1])
			plt.plot( [ int(i) % 128, int(j) % 128 ] , [ int(i)/128, int(j)/128 ], linewidth=1.5  )
		else:
			i = l[0]
			plt.plot( [ int(i) % 128 ] , [ int(i)/128 ], marker='s', markersize=2  )

plt.savefig("agg1.png")