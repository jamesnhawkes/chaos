from subprocess import Popen, PIPE
import sys

if len(sys.argv) > 1:
	FILE_NAME = sys.argv[1]
else:
	print ( "Usage: " + sys.argv[0] + " [filename] \n \t -- Do not include the file suffix in the filename")
	sys.exit(1)

INCLUDE_DIRECTORIES_LIGHT 	= [ "src" ]
INCLUDE_DIRECTORIES_FULL	= [ "matrices", "misc" ]

INCLUDE_FILES_LIGHT			= [ "LICENSE.md", "README.md" ]
INCLUDE_FILES_FULL			= [  ]

COMMAND_LIGHT = [ 'tar', '-cf', FILE_NAME+"-light.tar.gz" ] 		+ INCLUDE_FILES_LIGHT + INCLUDE_DIRECTORIES_LIGHT
COMMAND_FULL = [ 'tar', '-cf', FILE_NAME+".tar.gz" ]	+ INCLUDE_FILES_LIGHT + INCLUDE_FILES_FULL + INCLUDE_DIRECTORIES_LIGHT + INCLUDE_DIRECTORIES_FULL

#print (COMMAND_LIGHT)
Popen(COMMAND_LIGHT, shell=True ) 
Popen(COMMAND_FULL, shell=True ) 